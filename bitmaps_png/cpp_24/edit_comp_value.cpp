
/* Do not modify this file, it was automatically generated by the
 * PNG2cpp CMake script, using a *.png file as input.
 */

#include <bitmaps_png/bitmaps_list.h>

static const unsigned char png[] = {
 0x89, 0x50, 0x4e, 0x47, 0x0d, 0x0a, 0x1a, 0x0a, 0x00, 0x00, 0x00, 0x0d, 0x49, 0x48, 0x44, 0x52,
 0x00, 0x00, 0x00, 0x18, 0x00, 0x00, 0x00, 0x18, 0x08, 0x06, 0x00, 0x00, 0x00, 0xe0, 0x77, 0x3d,
 0xf8, 0x00, 0x00, 0x02, 0x18, 0x49, 0x44, 0x41, 0x54, 0x48, 0xc7, 0x63, 0x60, 0x40, 0x02, 0xa1,
 0xa1, 0xa1, 0x2a, 0xc1, 0xc1, 0xc1, 0xaa, 0x0c, 0xb4, 0x02, 0x21, 0x21, 0x21, 0x6f, 0x81, 0xf8,
 0x37, 0x10, 0x37, 0x03, 0x2d, 0x63, 0xa3, 0x85, 0x05, 0xff, 0x91, 0xf0, 0x25, 0xa0, 0x25, 0xc6,
 0x34, 0xb1, 0x20, 0x39, 0x39, 0x19, 0x66, 0x09, 0x75, 0x7d, 0x03, 0xb3, 0x60, 0xdb, 0xb6, 0x6d,
 0xff, 0x9b, 0x9b, 0x9b, 0xff, 0x03, 0x0d, 0x86, 0x59, 0x74, 0x25, 0x28, 0x28, 0xc8, 0x94, 0x5c,
 0x43, 0x0f, 0xa3, 0x05, 0xcd, 0xff, 0x9d, 0x3b, 0x77, 0x82, 0xf1, 0xe2, 0xc5, 0x8b, 0xff, 0xa7,
 0xa4, 0xa4, 0x20, 0xfb, 0xa6, 0xc3, 0xd3, 0xd3, 0x93, 0x9d, 0x6a, 0x16, 0x80, 0x30, 0xb5, 0x7d,
 0x03, 0x36, 0x24, 0x21, 0x21, 0x01, 0x4e, 0x53, 0xdb, 0x37, 0x18, 0x3e, 0xa0, 0xaa, 0x6f, 0x08,
 0x59, 0x40, 0xb1, 0x6f, 0x88, 0xb5, 0x80, 0x6c, 0xdf, 0x90, 0x62, 0x01, 0x0c, 0x2f, 0x5c, 0xb8,
 0x10, 0x39, 0xdf, 0xa0, 0xe3, 0xc3, 0x83, 0xd7, 0x82, 0xad, 0x5b, 0xb7, 0x62, 0x04, 0x11, 0xb0,
 0xa0, 0x34, 0x21, 0x3a, 0x88, 0x40, 0x49, 0x74, 0xde, 0x86, 0xdd, 0xff, 0xa7, 0xac, 0xc1, 0xc4,
 0xcd, 0xb3, 0x57, 0xfd, 0x0f, 0xce, 0xae, 0xfa, 0xef, 0x1e, 0x9f, 0x07, 0xc2, 0x7f, 0x1c, 0x52,
 0x6b, 0x96, 0x28, 0x34, 0xec, 0x76, 0x90, 0x6a, 0x3a, 0x64, 0x8d, 0x8e, 0xf1, 0xe6, 0x03, 0xcd,
 0xd2, 0x65, 0xff, 0xa5, 0x1a, 0x8f, 0x50, 0x84, 0x71, 0xe6, 0x64, 0x90, 0x25, 0x2e, 0x13, 0x0e,
 0x82, 0x15, 0x49, 0x03, 0x71, 0xe6, 0x9a, 0x1b, 0xff, 0xeb, 0x76, 0xdc, 0xfb, 0xaf, 0xd0, 0x72,
 0x0c, 0x2c, 0x16, 0x38, 0xff, 0xd2, 0xff, 0x29, 0x47, 0x1e, 0xff, 0x4f, 0x5d, 0x7d, 0x1d, 0xcc,
 0xf7, 0x99, 0x7b, 0x11, 0xcc, 0xcf, 0x5b, 0x7f, 0x8b, 0x38, 0x0b, 0x40, 0x18, 0x66, 0x81, 0x4c,
 0xd3, 0x91, 0xff, 0x65, 0x9b, 0xef, 0xfc, 0x7f, 0xf7, 0xed, 0xf7, 0x7f, 0xf5, 0x8e, 0xe3, 0xff,
 0xcd, 0x27, 0x9e, 0xfe, 0x7f, 0xff, 0xed, 0xf7, 0xff, 0xc1, 0x0b, 0x2e, 0xff, 0xbf, 0xfe, 0xf2,
 0xeb, 0xff, 0xb0, 0x45, 0x57, 0xfe, 0x3f, 0xff, 0xf4, 0xf3, 0x7f, 0xe4, 0x92, 0x2b, 0xff, 0x4f,
 0x3e, 0xfc, 0xf8, 0x3f, 0x76, 0xd9, 0x55, 0x84, 0x05, 0xf8, 0x22, 0xd9, 0xae, 0x6b, 0x37, 0x8a,
 0x6b, 0x1e, 0xbd, 0xff, 0x01, 0xb6, 0x00, 0x84, 0x2d, 0x27, 0x9d, 0x01, 0x8b, 0x1d, 0xbc, 0xfb,
 0x1e, 0xec, 0x7a, 0x90, 0x2f, 0x41, 0x62, 0x7b, 0x6f, 0xbd, 0x03, 0x5b, 0x8c, 0xd7, 0x02, 0x58,
 0x0a, 0x41, 0x8f, 0x03, 0x98, 0x05, 0x30, 0x7e, 0xe9, 0xe6, 0xdb, 0xff, 0x37, 0x5d, 0x7d, 0x0d,
 0x36, 0x5c, 0xb6, 0xe9, 0xe8, 0xff, 0x95, 0x17, 0x5e, 0xfe, 0x3f, 0xfb, 0xf8, 0xd3, 0x7f, 0xc3,
 0xbe, 0x53, 0xf8, 0x2d, 0x80, 0xa5, 0x6b, 0xcd, 0xd2, 0xe5, 0xff, 0x70, 0x59, 0x90, 0xb8, 0xe2,
 0xda, 0xff, 0x63, 0xf7, 0x3f, 0xfc, 0x57, 0x6e, 0x3b, 0xf6, 0x5f, 0xb3, 0xf3, 0xc4, 0xff, 0xe4,
 0x95, 0x90, 0xb8, 0x68, 0xdb, 0xf3, 0xe0, 0x7f, 0xfd, 0xce, 0x7b, 0xf8, 0x2d, 0x80, 0x55, 0x99,
 0xb2, 0x75, 0x7b, 0x4e, 0x62, 0xb3, 0xc0, 0x6c, 0xc2, 0xe9, 0xff, 0xdf, 0x7f, 0xff, 0xfd, 0xbf,
 0xf1, 0xca, 0xeb, 0xff, 0x8b, 0xcf, 0x3e, 0x07, 0x27, 0x80, 0x2b, 0xcf, 0xbf, 0xfc, 0x5f, 0x7d,
 0xf1, 0xe5, 0xff, 0x9b, 0xaf, 0xbe, 0xfe, 0x77, 0x9d, 0x79, 0x1e, 0xa7, 0x05, 0x28, 0x95, 0xbe,
 0x54, 0xe3, 0xe1, 0x3d, 0xc8, 0x16, 0x98, 0x02, 0x0d, 0x06, 0x45, 0x38, 0x28, 0x25, 0x81, 0xc2,
 0x1b, 0x86, 0xb5, 0xba, 0x4e, 0xfc, 0x97, 0x6b, 0x3e, 0xfa, 0xdf, 0x71, 0xda, 0xb9, 0xff, 0x6a,
 0xed, 0xc7, 0x51, 0x53, 0x11, 0xbe, 0x66, 0x0b, 0xba, 0x05, 0xe4, 0x60, 0x00, 0x17, 0xea, 0xb1,
 0xe9, 0xa9, 0xb6, 0x90, 0xe4, 0x00, 0x00, 0x00, 0x00, 0x49, 0x45, 0x4e, 0x44, 0xae, 0x42, 0x60,
 0x82,
};

const BITMAP_OPAQUE edit_comp_value_xpm[1] = {{ png, sizeof( png ), "edit_comp_value_xpm" }};

//EOF
