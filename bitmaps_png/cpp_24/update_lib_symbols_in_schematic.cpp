
/* Do not modify this file, it was automatically generated by the
 * PNG2cpp CMake script, using a *.png file as input.
 */

#include <bitmaps_png/bitmaps_list.h>

static const unsigned char png[] = {
 0x89, 0x50, 0x4e, 0x47, 0x0d, 0x0a, 0x1a, 0x0a, 0x00, 0x00, 0x00, 0x0d, 0x49, 0x48, 0x44, 0x52,
 0x00, 0x00, 0x00, 0x18, 0x00, 0x00, 0x00, 0x18, 0x08, 0x06, 0x00, 0x00, 0x00, 0xe0, 0x77, 0x3d,
 0xf8, 0x00, 0x00, 0x02, 0xfd, 0x49, 0x44, 0x41, 0x54, 0x48, 0xc7, 0xad, 0x95, 0xef, 0x4b, 0x53,
 0x61, 0x14, 0xc7, 0xf7, 0x27, 0xf4, 0xa6, 0x17, 0xf5, 0xaa, 0x17, 0xe5, 0xb2, 0x2c, 0xaa, 0xf7,
 0x92, 0x5b, 0xa9, 0xa8, 0xa5, 0x51, 0x6e, 0xee, 0x17, 0xc1, 0x60, 0x45, 0x9a, 0xae, 0x08, 0x34,
 0x1c, 0x48, 0x0c, 0x59, 0xb2, 0x81, 0x85, 0x08, 0x51, 0x30, 0x84, 0xc0, 0x99, 0xdb, 0x74, 0x2d,
 0xa7, 0x9b, 0xbb, 0xc6, 0x36, 0xa5, 0x52, 0xe7, 0xcc, 0x90, 0x2c, 0x93, 0x20, 0x9d, 0x66, 0x45,
 0x2c, 0xf7, 0xcb, 0xca, 0x08, 0x4f, 0xcf, 0x73, 0x70, 0x6b, 0x3f, 0x65, 0xce, 0x3d, 0xf0, 0x65,
 0xf7, 0x9e, 0xdd, 0x73, 0x3e, 0xcf, 0x73, 0x7e, 0xdc, 0xcb, 0x62, 0x91, 0x65, 0xb7, 0xdb, 0xcf,
 0x13, 0x4d, 0x11, 0x6d, 0x10, 0x41, 0x16, 0xda, 0x60, 0x18, 0xc6, 0x3d, 0x3c, 0x3c, 0x5c, 0xc1,
 0x4a, 0x5c, 0x5b, 0xc1, 0x21, 0x57, 0x22, 0xa0, 0xf2, 0x44, 0x80, 0x27, 0x97, 0x00, 0xa2, 0x89,
 0x44, 0xc0, 0x46, 0x8e, 0x01, 0xbf, 0x12, 0x01, 0x90, 0x63, 0x6d, 0x6e, 0x0b, 0x68, 0x68, 0x68,
 0x80, 0xea, 0xea, 0xea, 0x38, 0xf1, 0xf9, 0x7c, 0x90, 0xc9, 0x64, 0xd0, 0xd6, 0xd6, 0x06, 0x03,
 0x03, 0x03, 0xd9, 0x01, 0xcc, 0x66, 0x33, 0x18, 0x8d, 0x46, 0x10, 0x8b, 0xc5, 0x18, 0xac, 0xb5,
 0xb5, 0x35, 0xaa, 0x96, 0x96, 0x16, 0xa8, 0xab, 0xab, 0x43, 0x98, 0x54, 0x2a, 0x05, 0x83, 0xc1,
 0xb0, 0x33, 0x80, 0x5e, 0xaf, 0x07, 0x1e, 0x8f, 0x17, 0xdd, 0x71, 0x53, 0x53, 0x53, 0x4a, 0x67,
 0xad, 0x56, 0x0b, 0x22, 0x91, 0x08, 0x37, 0x60, 0xb3, 0xd9, 0x32, 0x07, 0x58, 0xad, 0x56, 0x68,
 0x6f, 0x6f, 0x07, 0x8d, 0x46, 0x03, 0x02, 0x81, 0x20, 0x2d, 0x80, 0x4a, 0xa5, 0x52, 0xe1, 0x26,
 0xe4, 0x72, 0x39, 0xa8, 0xd5, 0x6a, 0x18, 0x1a, 0x1a, 0xda, 0x59, 0x0d, 0x24, 0x12, 0x49, 0x5a,
 0x40, 0x4f, 0x4f, 0x0f, 0xd6, 0x83, 0x4a, 0x28, 0x14, 0x22, 0xa8, 0xb1, 0xb1, 0x11, 0xba, 0xba,
 0xba, 0x40, 0xa7, 0xd3, 0xed, 0x1e, 0x40, 0x4f, 0x49, 0x83, 0xea, 0x1e, 0x3c, 0x84, 0xe7, 0xf5,
 0x0a, 0x30, 0x9f, 0x2a, 0x83, 0x91, 0xc3, 0x5c, 0x94, 0xf1, 0x44, 0x09, 0x30, 0xc4, 0xc6, 0xf4,
 0x3f, 0xcd, 0x1e, 0xd0, 0xdd, 0xdd, 0x0d, 0x8a, 0xb3, 0x95, 0xc0, 0xe4, 0x9f, 0x01, 0x67, 0x1e,
 0x27, 0xb5, 0x8e, 0x97, 0x82, 0xe3, 0x10, 0xe7, 0x62, 0x1c, 0x80, 0x16, 0xac, 0xb3, 0xb3, 0x13,
 0x3a, 0x3a, 0x3a, 0xf0, 0xe8, 0xe9, 0x00, 0x23, 0xaa, 0x7b, 0x30, 0x2d, 0xb9, 0x09, 0x33, 0xb2,
 0xdb, 0x30, 0x75, 0xe9, 0x1a, 0x78, 0xfb, 0xac, 0xe0, 0x9b, 0xfb, 0x00, 0xbe, 0xf7, 0x0b, 0x78,
 0xed, 0xae, 0xba, 0x12, 0x01, 0x6d, 0x46, 0x21, 0xd4, 0xb1, 0xb7, 0xb7, 0x37, 0xae, 0xef, 0x53,
 0x02, 0x0c, 0xfd, 0xe0, 0x28, 0x28, 0x81, 0x6f, 0x93, 0x33, 0x10, 0x5e, 0xf3, 0x43, 0x38, 0x18,
 0x84, 0xf5, 0xf5, 0x75, 0xf0, 0x7b, 0x57, 0x88, 0x3e, 0xe3, 0x75, 0x38, 0x14, 0x86, 0xb9, 0x66,
 0x35, 0x42, 0x5c, 0x79, 0x9c, 0xc0, 0xd8, 0xc1, 0xc2, 0xbd, 0xd1, 0x14, 0x99, 0x4c, 0xa6, 0xb4,
 0x73, 0xa0, 0x54, 0x2a, 0xe1, 0x51, 0x59, 0x0d, 0x3a, 0xae, 0xda, 0x47, 0x31, 0x58, 0x88, 0x40,
 0xbe, 0xb8, 0xc6, 0xc1, 0xc9, 0xe6, 0xa2, 0xdd, 0x23, 0xa8, 0x07, 0xff, 0xd2, 0x32, 0x01, 0x87,
 0x60, 0xa2, 0x5c, 0x1a, 0x81, 0x28, 0x93, 0x6a, 0x50, 0x5b, 0x5b, 0x9b, 0x34, 0xc9, 0x54, 0x66,
 0x52, 0x44, 0xea, 0xb4, 0xa8, 0x33, 0x23, 0x80, 0xea, 0xfb, 0xf4, 0x2c, 0xb8, 0x62, 0xea, 0xe1,
 0x11, 0xc9, 0xd1, 0xfe, 0xe9, 0x71, 0xdf, 0x96, 0x8d, 0xfb, 0x3a, 0x09, 0x40, 0xeb, 0x61, 0xb1,
 0x58, 0xe2, 0x44, 0x7b, 0xdd, 0x59, 0x50, 0x12, 0x0d, 0xf4, 0xb2, 0x90, 0x07, 0xae, 0x23, 0xc5,
 0x29, 0x8b, 0x3c, 0x7b, 0x43, 0x09, 0xe3, 0xa5, 0x97, 0x23, 0xf7, 0xa1, 0x8c, 0xdf, 0xa6, 0xce,
 0x93, 0xe5, 0xe8, 0xb4, 0x70, 0x5f, 0x0b, 0x5e, 0x93, 0x2d, 0x29, 0xf0, 0xe8, 0xd1, 0x62, 0x78,
 0x75, 0xba, 0x06, 0x3c, 0xfc, 0xeb, 0xb1, 0xf0, 0x9f, 0x19, 0x7f, 0x0f, 0x1c, 0x5c, 0x01, 0x3a,
 0x79, 0x8d, 0x83, 0x98, 0x86, 0x69, 0x92, 0x8e, 0x58, 0x80, 0x6f, 0xfe, 0x63, 0x34, 0x75, 0x2b,
 0x56, 0x47, 0xc4, 0xfe, 0x86, 0x02, 0xce, 0x65, 0x02, 0x18, 0xb9, 0x75, 0x07, 0x9d, 0xdc, 0x17,
 0xae, 0x62, 0x21, 0xd7, 0x16, 0x97, 0xc1, 0x23, 0xfc, 0x0f, 0x59, 0xd2, 0x5b, 0x20, 0xfc, 0x63,
 0x0d, 0x01, 0x5f, 0x5f, 0x4c, 0xa1, 0xcd, 0x91, 0xc7, 0x51, 0x44, 0x5a, 0x95, 0x42, 0x26, 0xb7,
 0x4d, 0x17, 0x6d, 0xd3, 0x63, 0xa5, 0xe8, 0x38, 0xa7, 0xd0, 0x20, 0x84, 0x06, 0x0b, 0xac, 0xac,
 0x62, 0xab, 0x62, 0x9b, 0x06, 0x02, 0xe0, 0x27, 0xf7, 0xbe, 0xb7, 0xf3, 0xd8, 0xa6, 0xce, 0x03,
 0x45, 0x7b, 0x58, 0x3b, 0x59, 0x4e, 0x76, 0x91, 0x98, 0x0e, 0x11, 0x85, 0x4c, 0x56, 0xca, 0xc0,
 0x6b, 0x18, 0x04, 0xdf, 0xbb, 0x05, 0x1c, 0xb6, 0xa5, 0x27, 0xcf, 0xf0, 0x74, 0xee, 0x2a, 0x19,
 0xfd, 0xfd, 0x4b, 0x06, 0xad, 0x82, 0x95, 0xcd, 0xda, 0x82, 0x04, 0xd3, 0xbe, 0x2a, 0xc8, 0x7f,
 0x2e, 0x36, 0x97, 0xc7, 0xda, 0xcd, 0x62, 0xd8, 0xc5, 0xfb, 0x49, 0x0a, 0x9a, 0x49, 0xb0, 0x19,
 0xa2, 0xdf, 0x44, 0x7f, 0x48, 0xcf, 0xcf, 0x92, 0x9c, 0xdf, 0x1d, 0xcb, 0x2f, 0xdc, 0x17, 0xfb,
 0xec, 0x3f, 0x2f, 0xa9, 0x78, 0x26, 0x1f, 0x41, 0x78, 0x8d, 0x00, 0x00, 0x00, 0x00, 0x49, 0x45,
 0x4e, 0x44, 0xae, 0x42, 0x60, 0x82,
};

const BITMAP_OPAQUE update_lib_symbols_in_schematic_xpm[1] = {{ png, sizeof( png ), "update_lib_symbols_in_schematic_xpm" }};

//EOF
