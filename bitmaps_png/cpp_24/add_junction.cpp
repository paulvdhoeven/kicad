
/* Do not modify this file, it was automatically generated by the
 * PNG2cpp CMake script, using a *.png file as input.
 */

#include <bitmaps_png/bitmaps_list.h>

static const unsigned char png[] = {
 0x89, 0x50, 0x4e, 0x47, 0x0d, 0x0a, 0x1a, 0x0a, 0x00, 0x00, 0x00, 0x0d, 0x49, 0x48, 0x44, 0x52,
 0x00, 0x00, 0x00, 0x18, 0x00, 0x00, 0x00, 0x18, 0x08, 0x06, 0x00, 0x00, 0x00, 0xe0, 0x77, 0x3d,
 0xf8, 0x00, 0x00, 0x00, 0x8f, 0x49, 0x44, 0x41, 0x54, 0x48, 0xc7, 0x63, 0x60, 0x20, 0x01, 0x84,
 0x86, 0x86, 0x9e, 0x02, 0x61, 0x06, 0x5a, 0x81, 0x90, 0x90, 0x90, 0xff, 0x20, 0x3c, 0x6a, 0xc1,
 0xa8, 0x05, 0x23, 0xd1, 0x82, 0x55, 0xda, 0xa1, 0x6c, 0xfb, 0x55, 0x9d, 0x26, 0xec, 0xd0, 0x74,
 0xfa, 0xbf, 0x55, 0xdb, 0xf9, 0xff, 0x7e, 0x35, 0xa7, 0x0e, 0x90, 0x18, 0xd5, 0x2c, 0x00, 0x19,
 0xbe, 0x5f, 0xcd, 0xf1, 0x3f, 0x2a, 0x76, 0xea, 0x20, 0xd6, 0xdb, 0xfb, 0x61, 0x5e, 0xc7, 0x85,
 0x77, 0x68, 0x3a, 0xff, 0x47, 0xb7, 0xe0, 0x80, 0x9a, 0xe3, 0x53, 0x62, 0x2d, 0xd8, 0x4b, 0x8e,
 0x05, 0x40, 0xfc, 0x9a, 0x6a, 0x41, 0xb4, 0x4f, 0xcd, 0xb1, 0x0f, 0x8b, 0x0f, 0x7a, 0xa9, 0x66,
 0xc1, 0x36, 0x15, 0x4f, 0x76, 0x60, 0x98, 0xf7, 0x6c, 0xd7, 0x72, 0xfe, 0xbf, 0x1d, 0x18, 0xd1,
 0x20, 0xc3, 0xcf, 0x18, 0x1b, 0xb3, 0x8e, 0xe6, 0x83, 0x51, 0x0b, 0x46, 0x2d, 0xa0, 0x01, 0x20,
 0xa7, 0xd9, 0x02, 0x00, 0xd7, 0x4e, 0xaf, 0x3d, 0x2f, 0x21, 0xbc, 0xbd, 0x00, 0x00, 0x00, 0x00,
 0x49, 0x45, 0x4e, 0x44, 0xae, 0x42, 0x60, 0x82,
};

const BITMAP_OPAQUE add_junction_xpm[1] = {{ png, sizeof( png ), "add_junction_xpm" }};

//EOF
