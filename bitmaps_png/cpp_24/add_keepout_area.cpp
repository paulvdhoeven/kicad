
/* Do not modify this file, it was automatically generated by the
 * PNG2cpp CMake script, using a *.png file as input.
 */

#include <bitmaps_png/bitmaps_list.h>

static const unsigned char png[] = {
 0x89, 0x50, 0x4e, 0x47, 0x0d, 0x0a, 0x1a, 0x0a, 0x00, 0x00, 0x00, 0x0d, 0x49, 0x48, 0x44, 0x52,
 0x00, 0x00, 0x00, 0x18, 0x00, 0x00, 0x00, 0x18, 0x08, 0x06, 0x00, 0x00, 0x00, 0xe0, 0x77, 0x3d,
 0xf8, 0x00, 0x00, 0x00, 0x79, 0x49, 0x44, 0x41, 0x54, 0x48, 0xc7, 0xed, 0xd6, 0xc1, 0x09, 0xc0,
 0x20, 0x0c, 0x05, 0xd0, 0x9f, 0x35, 0xdc, 0xa4, 0x10, 0xcf, 0x8e, 0x25, 0x8e, 0xe5, 0x59, 0xa1,
 0x9b, 0xb8, 0x86, 0xbd, 0x54, 0x5b, 0x4b, 0x4f, 0x25, 0x7a, 0x69, 0xfe, 0x59, 0xf2, 0x40, 0x21,
 0x5f, 0x32, 0x21, 0x27, 0x00, 0x8c, 0x2b, 0xb9, 0x78, 0xb6, 0xf8, 0x90, 0xb7, 0x59, 0x64, 0x42,
 0xae, 0x8f, 0x73, 0x92, 0x00, 0x3a, 0x50, 0x3c, 0x13, 0x04, 0xd3, 0xe6, 0x0e, 0x40, 0x8c, 0xb1,
 0x4a, 0x0c, 0x77, 0xce, 0xd1, 0x1d, 0xd8, 0x4f, 0x60, 0x13, 0x06, 0x12, 0x00, 0x0c, 0xd7, 0x22,
 0x09, 0xf4, 0x37, 0x50, 0x40, 0x01, 0x05, 0x14, 0xf8, 0x05, 0xd0, 0xd6, 0x6a, 0xf1, 0x6c, 0x85,
 0xd7, 0x75, 0x5e, 0x56, 0x38, 0x6b, 0x2a, 0x73, 0x66, 0xe9, 0x4f, 0xfd, 0xb6, 0x1c, 0xee, 0x0b,
 0x8c, 0xd6, 0x62, 0xb2, 0x0e, 0x75, 0x00, 0x00, 0x00, 0x00, 0x49, 0x45, 0x4e, 0x44, 0xae, 0x42,
 0x60, 0x82,
};

const BITMAP_OPAQUE add_keepout_area_xpm[1] = {{ png, sizeof( png ), "add_keepout_area_xpm" }};

//EOF
