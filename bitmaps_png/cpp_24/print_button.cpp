
/* Do not modify this file, it was automatically generated by the
 * PNG2cpp CMake script, using a *.png file as input.
 */

#include <bitmaps_png/bitmaps_list.h>

static const unsigned char png[] = {
 0x89, 0x50, 0x4e, 0x47, 0x0d, 0x0a, 0x1a, 0x0a, 0x00, 0x00, 0x00, 0x0d, 0x49, 0x48, 0x44, 0x52,
 0x00, 0x00, 0x00, 0x18, 0x00, 0x00, 0x00, 0x18, 0x08, 0x06, 0x00, 0x00, 0x00, 0xe0, 0x77, 0x3d,
 0xf8, 0x00, 0x00, 0x00, 0xd9, 0x49, 0x44, 0x41, 0x54, 0x48, 0xc7, 0x63, 0x60, 0xa0, 0x37, 0xd8,
 0xb9, 0x73, 0xe7, 0x7f, 0x4a, 0xf0, 0xa8, 0x05, 0xa4, 0x5b, 0x10, 0x18, 0x18, 0x28, 0x1c, 0x1c,
 0x1c, 0xbc, 0x22, 0x24, 0x24, 0xe4, 0x13, 0x10, 0xff, 0xa7, 0x12, 0x06, 0x99, 0xb5, 0x3c, 0x34,
 0x34, 0x54, 0x88, 0x01, 0x6a, 0x38, 0xc5, 0x86, 0x86, 0x06, 0x87, 0xfc, 0x8f, 0x0c, 0x0a, 0x06,
 0xb3, 0x03, 0xc2, 0xa2, 0x60, 0xe2, 0xcb, 0x18, 0xa8, 0xe5, 0xf2, 0x1a, 0x27, 0x9f, 0xff, 0xcb,
 0x0c, 0x5c, 0xc1, 0x6c, 0xd5, 0x8a, 0x75, 0xff, 0x1d, 0x53, 0xaa, 0xc0, 0x3e, 0x61, 0xa0, 0x56,
 0xb0, 0x84, 0x01, 0x71, 0x6c, 0x40, 0x10, 0x98, 0xed, 0x1f, 0x11, 0xfb, 0x3f, 0x38, 0x34, 0x0c,
 0xcc, 0x66, 0xa0, 0x62, 0xb8, 0x63, 0xc5, 0x70, 0x0b, 0x82, 0x82, 0x82, 0xfe, 0xfb, 0xf9, 0xf9,
 0xfd, 0x4f, 0x4b, 0x4b, 0xa3, 0x08, 0x83, 0xcc, 0x00, 0x99, 0x85, 0x61, 0x81, 0xbf, 0xbf, 0xff,
 0x7f, 0x17, 0x17, 0x97, 0xff, 0x6e, 0x6e, 0x6e, 0x14, 0x61, 0x90, 0x19, 0x20, 0xb3, 0x30, 0x2c,
 0x80, 0x61, 0x4a, 0xf3, 0x01, 0xce, 0x20, 0x1a, 0xb5, 0x00, 0xa7, 0x05, 0xa3, 0xa5, 0xe9, 0xf0,
 0xb0, 0x20, 0x7b, 0xee, 0xbe, 0xff, 0x2e, 0x13, 0x0e, 0xfe, 0x4f, 0x99, 0x7d, 0xe0, 0x26, 0x4d,
 0x2c, 0xf0, 0x9f, 0x72, 0xf0, 0xbf, 0x45, 0xcf, 0xe1, 0x8f, 0x19, 0x8b, 0x76, 0x8a, 0xd1, 0xc4,
 0x82, 0xcc, 0x79, 0xfb, 0xef, 0x67, 0xcd, 0xdf, 0x2f, 0xc1, 0x30, 0x10, 0x00, 0x00, 0x9e, 0x41,
 0x77, 0xf3, 0xe0, 0xfa, 0x0f, 0x0d, 0x00, 0x00, 0x00, 0x00, 0x49, 0x45, 0x4e, 0x44, 0xae, 0x42,
 0x60, 0x82,
};

const BITMAP_OPAQUE print_button_xpm[1] = {{ png, sizeof( png ), "print_button_xpm" }};

//EOF
