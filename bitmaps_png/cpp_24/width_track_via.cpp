
/* Do not modify this file, it was automatically generated by the
 * PNG2cpp CMake script, using a *.png file as input.
 */

#include <bitmaps_png/bitmaps_list.h>

static const unsigned char png[] = {
 0x89, 0x50, 0x4e, 0x47, 0x0d, 0x0a, 0x1a, 0x0a, 0x00, 0x00, 0x00, 0x0d, 0x49, 0x48, 0x44, 0x52,
 0x00, 0x00, 0x00, 0x18, 0x00, 0x00, 0x00, 0x18, 0x08, 0x06, 0x00, 0x00, 0x00, 0xe0, 0x77, 0x3d,
 0xf8, 0x00, 0x00, 0x02, 0x94, 0x49, 0x44, 0x41, 0x54, 0x48, 0xc7, 0xb5, 0x95, 0x4d, 0x68, 0x13,
 0x51, 0x14, 0x85, 0x9f, 0x56, 0x45, 0x10, 0x44, 0x45, 0xd1, 0xfc, 0x58, 0x54, 0xfc, 0xc3, 0x8d,
 0x0b, 0xd1, 0xa2, 0x0d, 0x18, 0x37, 0x76, 0x69, 0x99, 0x4c, 0x9a, 0x42, 0x40, 0x5d, 0xf8, 0x8b,
 0x15, 0x44, 0x03, 0x82, 0x35, 0x3a, 0x33, 0x5a, 0x14, 0xa5, 0xd2, 0x44, 0x2b, 0x74, 0xaf, 0x10,
 0x93, 0xa8, 0x5d, 0x14, 0x4a, 0x8a, 0x68, 0x4d, 0x27, 0x51, 0x30, 0x51, 0x14, 0xdc, 0xa8, 0x15,
 0x37, 0x15, 0x6d, 0x62, 0xdb, 0x44, 0x8c, 0x60, 0xb1, 0xe3, 0xb9, 0xe9, 0x0b, 0x0c, 0x69, 0x62,
 0x93, 0x38, 0x0e, 0x1c, 0x66, 0x92, 0x3b, 0x73, 0xbe, 0xf3, 0xde, 0xbb, 0xf3, 0x86, 0xb1, 0x7f,
 0x3d, 0x24, 0x6d, 0xae, 0x59, 0x51, 0xfb, 0xcd, 0xb2, 0xda, 0xc7, 0xfe, 0xc7, 0x61, 0x96, 0xa2,
 0xcd, 0x30, 0xd7, 0x2c, 0xb2, 0x3a, 0x5c, 0xd5, 0x83, 0xa2, 0x28, 0xee, 0x77, 0x3a, 0x9d, 0x6b,
 0xe9, 0x1a, 0xe7, 0xfa, 0xb2, 0x00, 0x59, 0x8d, 0x13, 0xc0, 0x2c, 0xc7, 0x4e, 0x56, 0x0b, 0x38,
 0x0b, 0xa5, 0xa0, 0x47, 0x50, 0x46, 0x10, 0x84, 0x3d, 0xfa, 0x3a, 0xa0, 0x36, 0xdb, 0x91, 0x4b,
 0x67, 0xa6, 0xcd, 0xd5, 0xf4, 0xca, 0xeb, 0x91, 0x45, 0x55, 0x0f, 0xdf, 0xed, 0x76, 0x2f, 0x76,
 0x38, 0x1c, 0x1f, 0x00, 0xd0, 0xa0, 0x49, 0x5c, 0x1f, 0xd3, 0x05, 0x78, 0xb1, 0xf5, 0x54, 0x4f,
 0x92, 0x00, 0x5b, 0x3c, 0x77, 0x7d, 0x76, 0xbb, 0x7d, 0x5e, 0xd5, 0x00, 0xa4, 0xac, 0x83, 0x91,
 0x1b, 0xc6, 0xd7, 0x70, 0x1e, 0x80, 0xbe, 0x42, 0x1d, 0xd0, 0x5e, 0xc1, 0xe9, 0xfa, 0x68, 0x91,
 0x87, 0xa6, 0xea, 0x2f, 0x3c, 0xfe, 0xe1, 0x10, 0xc5, 0x34, 0x46, 0x68, 0x32, 0x64, 0x51, 0xc9,
 0x08, 0xe0, 0xc1, 0x1d, 0x6d, 0x5d, 0xcf, 0x29, 0xbd, 0xed, 0xb0, 0x14, 0x05, 0xf0, 0x8e, 0x61,
 0x5d, 0x03, 0xc0, 0x76, 0x41, 0x74, 0x7e, 0xb6, 0xc8, 0xd1, 0x49, 0xab, 0xf4, 0xf4, 0x97, 0x80,
 0x51, 0x01, 0xb8, 0xd3, 0x30, 0x00, 0xa6, 0x2b, 0xd0, 0x78, 0xb4, 0x43, 0xa5, 0xf4, 0xbb, 0x8e,
 0x5f, 0x7d, 0x86, 0xf4, 0xbd, 0xf8, 0xef, 0x32, 0xce, 0xc3, 0xd0, 0x03, 0xe8, 0x22, 0x7e, 0x37,
 0x03, 0xba, 0xa0, 0x26, 0x40, 0xd3, 0x81, 0x13, 0x2d, 0x56, 0x69, 0x30, 0x67, 0x91, 0xa2, 0x53,
 0x42, 0x8b, 0x6b, 0x04, 0x66, 0x0d, 0x7c, 0xd1, 0x5b, 0xa1, 0x9f, 0xbc, 0x21, 0xbe, 0x50, 0x83,
 0xd4, 0xf8, 0x62, 0xa9, 0x57, 0x28, 0xfd, 0x3a, 0x6f, 0x7f, 0x1f, 0xcc, 0x3b, 0x8b, 0xa6, 0xaf,
 0x11, 0xe6, 0x63, 0x48, 0x4f, 0x23, 0x1b, 0xa5, 0x56, 0x2f, 0x69, 0x82, 0xee, 0xa0, 0xfe, 0xfe,
 0xcd, 0x7b, 0xbc, 0xa4, 0xac, 0xca, 0x50, 0x43, 0x99, 0xce, 0x5b, 0x4f, 0x2d, 0xeb, 0x72, 0xb9,
 0x36, 0xe2, 0xfa, 0xf4, 0x8c, 0x1b, 0xd6, 0x48, 0x4f, 0x96, 0xc0, 0x60, 0xe2, 0x6f, 0xe6, 0x50,
 0xa0, 0xe6, 0x45, 0xc4, 0x9e, 0xe2, 0xe5, 0x26, 0x03, 0x86, 0x6f, 0x5c, 0xf4, 0xba, 0xc3, 0x78,
 0x94, 0x00, 0xab, 0x94, 0xe8, 0x6e, 0xe3, 0x77, 0x46, 0x45, 0xf5, 0xf0, 0xf4, 0x71, 0xc3, 0xcd,
 0x31, 0xf7, 0x0b, 0x61, 0x3c, 0x42, 0x00, 0x93, 0x1c, 0x6f, 0xd2, 0xd7, 0x72, 0x37, 0x98, 0x25,
 0xe3, 0x67, 0x81, 0xac, 0x8f, 0x8d, 0x73, 0x85, 0x33, 0x3e, 0xb6, 0xa1, 0xda, 0xf4, 0x6d, 0x7c,
 0xdb, 0x4d, 0x32, 0x4d, 0x9b, 0xa3, 0x37, 0x87, 0x61, 0x0a, 0xd2, 0x8a, 0xf4, 0x2d, 0xe7, 0x67,
 0xd6, 0x8a, 0xcc, 0xb7, 0xf5, 0x24, 0xe6, 0xc3, 0xfc, 0x13, 0x07, 0xec, 0xd3, 0xd7, 0x90, 0xf4,
 0x1e, 0x19, 0xa6, 0xba, 0x97, 0x6b, 0xc9, 0xd0, 0x39, 0xed, 0x65, 0xd0, 0xa3, 0xa5, 0x6f, 0x2d,
 0x2d, 0x40, 0x42, 0x95, 0xa6, 0x3f, 0xc4, 0xe7, 0xfe, 0x2d, 0x7d, 0x02, 0xf5, 0x35, 0x3e, 0x25,
 0x79, 0xf3, 0x48, 0x24, 0x92, 0x57, 0x22, 0x7c, 0xbe, 0x00, 0xc8, 0xce, 0xee, 0x1e, 0x0c, 0xd6,
 0x21, 0xf5, 0xbb, 0x3c, 0x40, 0x89, 0xb5, 0x16, 0x97, 0x61, 0x32, 0x41, 0x66, 0x94, 0xbc, 0x00,
 0x48, 0x86, 0xdb, 0x0b, 0x80, 0xef, 0xb3, 0xfa, 0x9b, 0x64, 0xd5, 0xcd, 0xd3, 0xbf, 0x27, 0xd8,
 0x0c, 0x40, 0x17, 0xbb, 0x4f, 0x66, 0xe9, 0xee, 0x65, 0x48, 0xee, 0xd5, 0x12, 0xa1, 0x76, 0x2d,
 0x75, 0x7b, 0xc5, 0x34, 0xc0, 0xcf, 0x1e, 0xce, 0x3e, 0x3d, 0xb2, 0xfa, 0x26, 0xff, 0xd1, 0x56,
 0x62, 0x07, 0x4b, 0xd5, 0x01, 0xd8, 0x04, 0xb3, 0xb1, 0x12, 0x8b, 0x3c, 0x4e, 0xb5, 0x4a, 0x00,
 0xaf, 0xa0, 0xd7, 0xb4, 0xd0, 0xe5, 0xee, 0xc9, 0x75, 0xb2, 0xd5, 0xd4, 0x9a, 0x34, 0x25, 0x5c,
 0xbd, 0xd9, 0x9b, 0x6c, 0x73, 0x25, 0xeb, 0xfb, 0x07, 0xae, 0xdf, 0xa8, 0x89, 0xa7, 0x06, 0xc0,
 0xd8, 0x00, 0x00, 0x00, 0x00, 0x49, 0x45, 0x4e, 0x44, 0xae, 0x42, 0x60, 0x82,
};

const BITMAP_OPAQUE width_track_via_xpm[1] = {{ png, sizeof( png ), "width_track_via_xpm" }};

//EOF
