
/* Do not modify this file, it was automatically generated by the
 * PNG2cpp CMake script, using a *.png file as input.
 */

#include <bitmaps_png/bitmaps_list.h>

static const unsigned char png[] = {
 0x89, 0x50, 0x4e, 0x47, 0x0d, 0x0a, 0x1a, 0x0a, 0x00, 0x00, 0x00, 0x0d, 0x49, 0x48, 0x44, 0x52,
 0x00, 0x00, 0x00, 0x18, 0x00, 0x00, 0x00, 0x18, 0x08, 0x04, 0x00, 0x00, 0x00, 0x4a, 0x7e, 0xf5,
 0x73, 0x00, 0x00, 0x01, 0x6a, 0x49, 0x44, 0x41, 0x54, 0x38, 0xcb, 0xbd, 0x93, 0xbf, 0x4a, 0x5c,
 0x51, 0x10, 0x87, 0x87, 0x15, 0x4d, 0xda, 0xb8, 0x5a, 0x07, 0x12, 0xbb, 0xb0, 0x8f, 0x91, 0x80,
 0x16, 0xde, 0x5c, 0xe7, 0x9b, 0x5d, 0x34, 0x4d, 0x2a, 0x9f, 0x63, 0xab, 0x54, 0x82, 0x10, 0x7b,
 0x2b, 0x51, 0x31, 0x4f, 0x20, 0xae, 0xef, 0x60, 0x95, 0x90, 0x08, 0xd7, 0xe8, 0x96, 0x8a, 0xd9,
 0xac, 0xff, 0x10, 0xc4, 0x9f, 0xc5, 0x2e, 0x5e, 0xd9, 0xeb, 0x5d, 0xb1, 0x71, 0xa6, 0x19, 0xe6,
 0xcc, 0x77, 0x7e, 0xe7, 0x9c, 0x99, 0x63, 0x76, 0x6f, 0x5f, 0x5f, 0xf3, 0x85, 0x2d, 0x32, 0x6e,
 0xb8, 0x8e, 0x3d, 0xd6, 0x48, 0x9a, 0x15, 0x2b, 0x37, 0x3e, 0x91, 0x21, 0x84, 0x38, 0xe7, 0xb2,
 0x1f, 0xed, 0x07, 0x65, 0xe5, 0x4b, 0x08, 0xc5, 0x8e, 0xfb, 0xc2, 0x1b, 0x33, 0xb3, 0xb4, 0x1a,
 0x0d, 0x76, 0x11, 0xe2, 0xfb, 0x23, 0x3a, 0x7c, 0x43, 0x9c, 0x90, 0x14, 0xf2, 0x09, 0x1d, 0xc4,
 0xf2, 0x40, 0xda, 0xeb, 0x88, 0x93, 0xa8, 0x3d, 0xa6, 0xec, 0x1f, 0xf8, 0x87, 0x22, 0x7d, 0x90,
 0x8a, 0x31, 0xda, 0x28, 0x66, 0xcd, 0xb6, 0x55, 0x74, 0x33, 0x12, 0xc4, 0xe1, 0xe2, 0x68, 0xbe,
 0xc7, 0x3c, 0xa2, 0x65, 0x56, 0x06, 0x98, 0xc5, 0x0e, 0x8a, 0x46, 0x7e, 0xce, 0x1f, 0xc8, 0xeb,
 0xc3, 0x00, 0x02, 0xb1, 0x9e, 0x03, 0x19, 0x9a, 0x9f, 0x18, 0xaa, 0x30, 0x89, 0xd8, 0xcf, 0x81,
 0x63, 0xce, 0x7a, 0x51, 0x19, 0x60, 0x46, 0x97, 0xcb, 0x1c, 0xf8, 0xcf, 0xf9, 0x93, 0xc0, 0x7d,
 0x8d, 0x99, 0xf9, 0x4f, 0x94, 0x56, 0x87, 0x01, 0x69, 0x15, 0xf1, 0x27, 0x57, 0x58, 0x45, 0xbd,
 0x01, 0x28, 0x03, 0xbc, 0x8e, 0x7c, 0x33, 0xef, 0xc3, 0xc2, 0x33, 0x9f, 0x75, 0xfa, 0x15, 0x6d,
 0x44, 0x52, 0x06, 0xc4, 0x2c, 0xf2, 0x83, 0x18, 0x7b, 0xd8, 0x6b, 0x10, 0xa7, 0x51, 0xdb, 0x1a,
 0x19, 0xf4, 0x66, 0x25, 0x6a, 0x74, 0x06, 0x46, 0xc3, 0xcc, 0xcc, 0x57, 0x10, 0xa7, 0xc5, 0xe1,
 0xf3, 0xcf, 0x74, 0x90, 0x77, 0x1b, 0xef, 0x07, 0x16, 0x9a, 0x15, 0x96, 0xb8, 0x45, 0xb4, 0xbc,
 0x1e, 0xe3, 0xfd, 0x66, 0x05, 0x2d, 0x84, 0xe8, 0x22, 0x3f, 0x2a, 0x20, 0x66, 0xf1, 0x91, 0xbf,
 0xfd, 0x6f, 0x73, 0x46, 0xb7, 0x17, 0xf9, 0x41, 0xa4, 0x73, 0x6f, 0xc9, 0x10, 0xed, 0x98, 0x2a,
 0x22, 0x23, 0x3e, 0xc3, 0x06, 0x19, 0x57, 0x5c, 0x90, 0xb1, 0x41, 0xf4, 0xae, 0x3a, 0x04, 0x29,
 0xb3, 0x97, 0x41, 0xfc, 0x9d, 0x1f, 0x21, 0x7e, 0x99, 0x3d, 0x07, 0xf9, 0xcd, 0xf6, 0x1d, 0xec,
 0x0f, 0x71, 0x9e, 0xeb, 0xa6, 0xe2, 0xf7, 0x00, 0x00, 0x00, 0x00, 0x49, 0x45, 0x4e, 0x44, 0xae,
 0x42, 0x60, 0x82,
};

const BITMAP_OPAQUE zoom_fit_in_page_xpm[1] = {{ png, sizeof( png ), "zoom_fit_in_page_xpm" }};

//EOF
