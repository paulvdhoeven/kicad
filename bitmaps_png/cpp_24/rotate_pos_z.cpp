
/* Do not modify this file, it was automatically generated by the
 * PNG2cpp CMake script, using a *.png file as input.
 */

#include <bitmaps_png/bitmaps_list.h>

static const unsigned char png[] = {
 0x89, 0x50, 0x4e, 0x47, 0x0d, 0x0a, 0x1a, 0x0a, 0x00, 0x00, 0x00, 0x0d, 0x49, 0x48, 0x44, 0x52,
 0x00, 0x00, 0x00, 0x18, 0x00, 0x00, 0x00, 0x18, 0x08, 0x06, 0x00, 0x00, 0x00, 0xe0, 0x77, 0x3d,
 0xf8, 0x00, 0x00, 0x01, 0xf6, 0x49, 0x44, 0x41, 0x54, 0x48, 0xc7, 0x63, 0x60, 0x20, 0x01, 0x1c,
 0x50, 0x73, 0x3c, 0x0a, 0xc4, 0x27, 0x19, 0x68, 0x05, 0xf6, 0xab, 0x39, 0x7e, 0x06, 0x5a, 0xf0,
 0x73, 0xd4, 0x02, 0x30, 0x88, 0x88, 0x88, 0x90, 0x0d, 0x0a, 0x0a, 0x92, 0xc7, 0x66, 0x41, 0x70,
 0x70, 0xb0, 0x61, 0x48, 0x48, 0x48, 0x39, 0x14, 0x37, 0x00, 0x71, 0x47, 0x68, 0x68, 0x68, 0x2c,
 0x51, 0x06, 0x47, 0x45, 0x45, 0x09, 0x02, 0x35, 0xac, 0x04, 0xe2, 0x37, 0x40, 0x83, 0xe2, 0x71,
 0xf9, 0x00, 0x28, 0xe7, 0x02, 0x34, 0xf4, 0x32, 0x50, 0xdd, 0x7f, 0x28, 0xde, 0xe5, 0xe9, 0xe9,
 0xc9, 0x4e, 0xd0, 0x02, 0xa0, 0xc6, 0x15, 0x40, 0x3c, 0x19, 0xa8, 0x99, 0x93, 0x50, 0x10, 0x39,
 0x38, 0x38, 0xb0, 0x00, 0xd5, 0xa5, 0x01, 0x0d, 0x7f, 0x09, 0xa4, 0x8f, 0x01, 0xe9, 0xc5, 0x04,
 0x2d, 0x00, 0x2a, 0xda, 0x09, 0x54, 0x9c, 0x4f, 0x4a, 0x1c, 0x00, 0xd5, 0xf3, 0x00, 0x83, 0xd3,
 0x0e, 0xe8, 0xb0, 0x9b, 0x04, 0x2d, 0x00, 0x2a, 0x56, 0x81, 0x7a, 0xfd, 0x30, 0x10, 0xe7, 0x06,
 0x06, 0x06, 0xea, 0x00, 0x85, 0x19, 0xb1, 0x59, 0x00, 0x54, 0xc7, 0x0f, 0x0a, 0x2a, 0x20, 0x9e,
 0x00, 0x54, 0xfb, 0x0a, 0xc8, 0x0f, 0x24, 0x2a, 0x1e, 0x80, 0x0a, 0x99, 0x81, 0x9a, 0x02, 0x80,
 0x78, 0x26, 0xc8, 0x55, 0x40, 0xcd, 0xdf, 0x81, 0xf4, 0xa3, 0x2c, 0xef, 0xc0, 0xbf, 0xd9, 0x5e,
 0x01, 0xff, 0x80, 0xfc, 0x2b, 0x20, 0x3e, 0x90, 0xfe, 0x08, 0xc4, 0xfb, 0x81, 0xb8, 0x1e, 0xe8,
 0x03, 0x19, 0xb2, 0x53, 0x54, 0x42, 0x42, 0x02, 0x07, 0x28, 0x55, 0xad, 0x30, 0x70, 0xfd, 0xba,
 0xdc, 0xc0, 0xe5, 0x17, 0xd0, 0x01, 0x5a, 0x20, 0x3e, 0xc8, 0x21, 0x54, 0xcd, 0x07, 0xfb, 0xd4,
 0x9d, 0xbe, 0xee, 0x57, 0x73, 0xf8, 0x05, 0x74, 0xbd, 0x12, 0x31, 0xd8, 0xcf, 0xcf, 0x8f, 0x97,
 0x68, 0xc3, 0x81, 0x41, 0xe0, 0xb8, 0x53, 0xc3, 0xe9, 0xff, 0x1e, 0x75, 0xc7, 0xff, 0x48, 0x49,
 0x93, 0x10, 0xbe, 0x4d, 0x6c, 0x9c, 0xf8, 0x81, 0xe2, 0xa1, 0xdf, 0xda, 0xeb, 0xff, 0x24, 0x0b,
 0x8f, 0x2f, 0x40, 0xf6, 0x19, 0x62, 0x30, 0x28, 0xb9, 0x13, 0x63, 0x78, 0x14, 0x50, 0xf1, 0x2f,
 0xa8, 0x8b, 0x3a, 0xa8, 0x1a, 0xe6, 0x40, 0x03, 0xb3, 0x80, 0xf8, 0x2f, 0x10, 0xff, 0x03, 0xba,
 0xa6, 0x90, 0xda, 0x86, 0x97, 0x43, 0x5d, 0xfd, 0x07, 0x68, 0x78, 0x12, 0x92, 0x78, 0x0d, 0xc8,
 0x27, 0xc8, 0x18, 0x5b, 0xae, 0xc7, 0x07, 0x18, 0x81, 0x9a, 0xba, 0xa1, 0x86, 0xff, 0x00, 0x1a,
 0x1e, 0x8c, 0x66, 0x31, 0xcc, 0x82, 0x1e, 0x90, 0x3c, 0x10, 0x5f, 0x4a, 0x4b, 0x4b, 0x63, 0x25,
 0x25, 0x83, 0xcd, 0x81, 0x1a, 0xfe, 0x05, 0xc8, 0x77, 0xc5, 0xe3, 0xc3, 0xa9, 0x40, 0xfc, 0x3a,
 0x3c, 0x3c, 0x5c, 0x99, 0x58, 0xc3, 0xd9, 0x80, 0x1a, 0x56, 0x43, 0x0d, 0x7f, 0x07, 0xe4, 0x5b,
 0xe2, 0x31, 0xbc, 0x00, 0x94, 0xaa, 0xf0, 0xa9, 0x41, 0x01, 0xb1, 0xb1, 0xb1, 0xdc, 0x40, 0xc5,
 0x3b, 0xa0, 0x86, 0x3f, 0x07, 0xfa, 0x42, 0x0f, 0x8f, 0x43, 0x3c, 0x80, 0x6a, 0x7e, 0x03, 0x71,
 0x34, 0x51, 0x86, 0x07, 0x04, 0x04, 0x08, 0x00, 0x15, 0x1f, 0x85, 0x1a, 0x7e, 0x0f, 0x54, 0xd8,
 0x11, 0x88, 0xfc, 0x77, 0xd0, 0x64, 0x7b, 0x17, 0x86, 0x41, 0x15, 0x10, 0xbe, 0xa0, 0x39, 0x0e,
 0x35, 0xfc, 0x12, 0x30, 0xb7, 0x4a, 0x12, 0x11, 0x94, 0x8a, 0xe8, 0x45, 0x02, 0xa8, 0xac, 0xc2,
 0x5b, 0xfe, 0x83, 0x30, 0xa8, 0x26, 0xa3, 0x45, 0x3d, 0x0e, 0x00, 0xca, 0xd4, 0x3e, 0xde, 0x51,
 0xc6, 0x34, 0x84, 0x00, 0x00, 0x00, 0x00, 0x49, 0x45, 0x4e, 0x44, 0xae, 0x42, 0x60, 0x82,
};

const BITMAP_OPAQUE rotate_pos_z_xpm[1] = {{ png, sizeof( png ), "rotate_pos_z_xpm" }};

//EOF
