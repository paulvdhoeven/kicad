
/* Do not modify this file, it was automatically generated by the
 * PNG2cpp CMake script, using a *.png file as input.
 */

#include <bitmaps_png/bitmaps_list.h>

static const unsigned char png[] = {
 0x89, 0x50, 0x4e, 0x47, 0x0d, 0x0a, 0x1a, 0x0a, 0x00, 0x00, 0x00, 0x0d, 0x49, 0x48, 0x44, 0x52,
 0x00, 0x00, 0x00, 0x18, 0x00, 0x00, 0x00, 0x18, 0x08, 0x06, 0x00, 0x00, 0x00, 0xe0, 0x77, 0x3d,
 0xf8, 0x00, 0x00, 0x02, 0x51, 0x49, 0x44, 0x41, 0x54, 0x48, 0xc7, 0x63, 0x90, 0x6c, 0x3a, 0xb2,
 0x55, 0xaa, 0xf1, 0xc8, 0x19, 0xf1, 0x86, 0xa3, 0x62, 0x0c, 0x54, 0x04, 0x21, 0x21, 0x21, 0x0e,
 0xc1, 0xc1, 0xc1, 0xd7, 0x18, 0x40, 0x86, 0x03, 0xf1, 0x7f, 0xe9, 0xc6, 0x23, 0x97, 0xa9, 0x65,
 0x49, 0x68, 0x68, 0xa8, 0x0d, 0xd0, 0x82, 0x97, 0x40, 0xda, 0x89, 0x41, 0xa2, 0xed, 0x90, 0x28,
 0xd0, 0x82, 0x4b, 0x20, 0x4b, 0x80, 0xf8, 0xba, 0x7c, 0xeb, 0x21, 0x49, 0xaa, 0x19, 0x0e, 0x03,
 0xd4, 0xb2, 0x04, 0xab, 0xe1, 0xd4, 0xb2, 0x04, 0xaf, 0xe1, 0xf8, 0x2c, 0xf9, 0x3c, 0x9d, 0x41,
 0xec, 0xe3, 0x44, 0x86, 0x96, 0x4f, 0x13, 0x19, 0x2e, 0x00, 0xf1, 0x17, 0x28, 0xbe, 0x00, 0x12,
 0xfb, 0x3c, 0x89, 0x41, 0x94, 0x68, 0xc3, 0x61, 0x00, 0x14, 0xd1, 0xa0, 0x08, 0x07, 0x59, 0x92,
 0xdd, 0x5e, 0x7d, 0x17, 0x68, 0xd8, 0x27, 0x20, 0xfe, 0x8f, 0x0d, 0x7f, 0x9c, 0xc8, 0xf8, 0x65,
 0x79, 0x89, 0x4e, 0x3d, 0xd1, 0x86, 0x23, 0xfb, 0x24, 0xab, 0xbd, 0xea, 0xfe, 0xc7, 0x09, 0x8c,
 0xff, 0x71, 0x19, 0x0e, 0xc2, 0x67, 0x9b, 0x45, 0xfe, 0xc7, 0x47, 0x87, 0xfe, 0x6f, 0x6e, 0x6e,
 0x6e, 0x20, 0x29, 0xb2, 0x40, 0xc1, 0x82, 0xcd, 0xe5, 0xef, 0xa6, 0xf0, 0xfe, 0x7f, 0x37, 0x99,
 0x07, 0x6e, 0x78, 0x4c, 0x78, 0xe0, 0xd7, 0x8a, 0x8a, 0x8a, 0xb4, 0x9d, 0x3b, 0x77, 0xbe, 0xda,
 0xb6, 0x6d, 0x9b, 0x28, 0xd1, 0x16, 0x40, 0xc3, 0x1c, 0xc5, 0xe0, 0x53, 0x6b, 0x1a, 0xfe, 0xef,
 0xdc, 0xb9, 0x03, 0x88, 0x77, 0xfe, 0xdf, 0x30, 0xa3, 0xf8, 0x7f, 0x4c, 0x44, 0xd0, 0x37, 0x58,
 0xb0, 0xec, 0xda, 0xb5, 0x6b, 0xf2, 0x8e, 0x1d, 0x3b, 0x9a, 0x88, 0xb6, 0x00, 0x68, 0xe8, 0x45,
 0x64, 0x0b, 0x4e, 0xaf, 0x6d, 0x00, 0x1b, 0x0c, 0xc2, 0x0b, 0x17, 0x2e, 0xfc, 0x1f, 0x17, 0x13,
 0xf9, 0x7f, 0xda, 0xb4, 0x69, 0xfb, 0x61, 0xea, 0x81, 0xe2, 0xe6, 0x40, 0x7c, 0x9e, 0x14, 0x0b,
 0xbe, 0xc0, 0x5d, 0x3f, 0x95, 0x1f, 0xee, 0x72, 0x88, 0xe1, 0x11, 0xff, 0x4a, 0x4b, 0x4b, 0x53,
 0x80, 0xfc, 0xf7, 0xfb, 0xf7, 0xef, 0xe7, 0x80, 0x5a, 0xc0, 0x0d, 0xc4, 0x9f, 0xc9, 0xb2, 0xe0,
 0xfd, 0x14, 0x1e, 0x64, 0x97, 0xff, 0xcb, 0xcd, 0xcd, 0x8d, 0x04, 0x86, 0x37, 0x3b, 0xa5, 0x16,
 0x5c, 0x40, 0x0e, 0x22, 0x50, 0x98, 0x83, 0x82, 0x25, 0x3f, 0x3f, 0x3f, 0x6a, 0xd5, 0xaa, 0x55,
 0x6c, 0xbb, 0x16, 0x2d, 0xdb, 0xb4, 0x6b, 0xc2, 0xf4, 0x97, 0x3b, 0x95, 0xdd, 0xc4, 0xc8, 0x0a,
 0x22, 0xe4, 0x48, 0x86, 0xa5, 0x16, 0x50, 0x98, 0x03, 0x0d, 0xf9, 0x08, 0xc4, 0xef, 0x40, 0x86,
 0xef, 0xd3, 0xf7, 0xfc, 0xbf, 0x5f, 0xcd, 0xf1, 0x0a, 0xc8, 0x12, 0xa0, 0xd8, 0x24, 0x92, 0x22,
 0x19, 0x9c, 0x4c, 0x27, 0xb3, 0xfe, 0x00, 0x19, 0x1e, 0x1d, 0x1e, 0xf0, 0x05, 0x96, 0x5a, 0x40,
 0x41, 0x02, 0xc2, 0x87, 0x54, 0x6c, 0x45, 0xf7, 0xab, 0x39, 0x5d, 0x02, 0x5a, 0xf0, 0x7f, 0x9f,
 0x6b, 0xf4, 0xbd, 0x5d, 0x3b, 0x76, 0xbc, 0x21, 0x29, 0x99, 0x82, 0xc0, 0xf2, 0x1a, 0xeb, 0xbe,
 0xf8, 0xe8, 0x90, 0x7f, 0xd5, 0xd5, 0xd5, 0x69, 0xd8, 0xe4, 0x41, 0x96, 0xec, 0x75, 0x8b, 0xb9,
 0xb5, 0x6b, 0xc5, 0xea, 0xff, 0x7b, 0x63, 0x72, 0x9e, 0x1c, 0xd2, 0xb4, 0x95, 0x24, 0xb9, 0xe0,
 0x02, 0xe5, 0x50, 0x50, 0x26, 0x02, 0x05, 0x01, 0x34, 0x9c, 0xb9, 0x37, 0x6f, 0xde, 0xcc, 0xb5,
 0x7b, 0xf7, 0x6e, 0x0b, 0xb0, 0x18, 0xd0, 0xe5, 0x7b, 0x62, 0x72, 0x1f, 0x82, 0x7c, 0x02, 0xc4,
 0xd7, 0x89, 0xb2, 0x04, 0xbd, 0xe0, 0x02, 0x79, 0x1d, 0x68, 0x58, 0x33, 0x10, 0x9f, 0x05, 0xe2,
 0x2f, 0x50, 0x7c, 0x16, 0x14, 0xe6, 0x20, 0x39, 0xe4, 0xe0, 0x22, 0x68, 0x09, 0x49, 0xa5, 0x22,
 0x5a, 0x70, 0x11, 0xb4, 0x84, 0x5c, 0xc3, 0x89, 0xb2, 0x84, 0x52, 0xc3, 0xf1, 0x5a, 0x42, 0x2d,
 0xc3, 0xe1, 0xe5, 0x12, 0x30, 0x5f, 0xec, 0x57, 0x75, 0xbc, 0x0c, 0xb2, 0xe4, 0x80, 0x9a, 0xe3,
 0x49, 0x50, 0xf3, 0xe2, 0x6a, 0x50, 0x50, 0x90, 0x23, 0x35, 0x9b, 0x2c, 0x60, 0x4b, 0xd4, 0x1c,
 0xcf, 0x00, 0x2d, 0xd8, 0x0b, 0x00, 0x1d, 0x61, 0x2c, 0xe6, 0x27, 0xbe, 0x50, 0x20, 0x00, 0x00,
 0x00, 0x00, 0x49, 0x45, 0x4e, 0x44, 0xae, 0x42, 0x60, 0x82,
};

const BITMAP_OPAQUE via_sketch_xpm[1] = {{ png, sizeof( png ), "via_sketch_xpm" }};

//EOF
