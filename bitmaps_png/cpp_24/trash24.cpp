
/* Do not modify this file, it was automatically generated by the
 * PNG2cpp CMake script, using a *.png file as input.
 */

#include <bitmaps_png/bitmaps_list.h>

static const unsigned char png[] = {
 0x89, 0x50, 0x4e, 0x47, 0x0d, 0x0a, 0x1a, 0x0a, 0x00, 0x00, 0x00, 0x0d, 0x49, 0x48, 0x44, 0x52,
 0x00, 0x00, 0x00, 0x18, 0x00, 0x00, 0x00, 0x18, 0x08, 0x04, 0x00, 0x00, 0x00, 0x4a, 0x7e, 0xf5,
 0x73, 0x00, 0x00, 0x02, 0x24, 0x49, 0x44, 0x41, 0x54, 0x38, 0xcb, 0x95, 0xd4, 0x4f, 0x48, 0xd4,
 0x41, 0x14, 0xc0, 0xf1, 0x9f, 0xda, 0xa1, 0x4b, 0x74, 0xea, 0x16, 0x1d, 0x84, 0x88, 0x6e, 0x39,
 0x6f, 0x66, 0x17, 0xff, 0x2f, 0xea, 0x41, 0x53, 0xb2, 0xc0, 0xd5, 0x8c, 0x30, 0x94, 0x34, 0x8a,
 0xc4, 0x4a, 0x08, 0x69, 0x29, 0x05, 0xed, 0x64, 0x16, 0x9a, 0x99, 0x97, 0x14, 0x82, 0x8a, 0x2e,
 0xb6, 0x48, 0x48, 0x52, 0x10, 0x24, 0x09, 0x05, 0x21, 0x54, 0x07, 0xb1, 0x24, 0x2c, 0x16, 0xa4,
 0x14, 0xe1, 0x15, 0x42, 0xb4, 0xfe, 0xbe, 0x1d, 0xdc, 0xa2, 0x65, 0x7f, 0x07, 0x9b, 0xb9, 0x3c,
 0x78, 0x7c, 0x60, 0xde, 0xbc, 0x37, 0xe3, 0x79, 0xff, 0xbf, 0x6c, 0x93, 0xbc, 0x32, 0x53, 0x32,
 0x62, 0x2f, 0x4a, 0xd4, 0x15, 0x18, 0xb1, 0xfb, 0x5c, 0x6e, 0xde, 0xae, 0xf0, 0xee, 0x03, 0x7b,
 0x8d, 0xb8, 0x52, 0x7b, 0xd0, 0xb4, 0xda, 0x1e, 0x3b, 0x26, 0xd3, 0xe6, 0xb5, 0x69, 0xf6, 0x3c,
 0xcf, 0xf3, 0x64, 0x4d, 0xd8, 0xe2, 0xfe, 0xbe, 0x09, 0x26, 0xb6, 0x0a, 0xec, 0xe4, 0xe6, 0x99,
 0xb2, 0xed, 0x79, 0xe1, 0x2a, 0xbd, 0x9c, 0xa5, 0x11, 0x4b, 0x84, 0x2a, 0x22, 0x54, 0x50, 0x43,
 0x31, 0x45, 0xb4, 0xd1, 0xc5, 0x75, 0xce, 0x21, 0x98, 0x32, 0x2f, 0x3b, 0x55, 0x85, 0xcb, 0x15,
 0xee, 0xa1, 0x28, 0x4a, 0x39, 0x3d, 0xa9, 0x48, 0xa9, 0xa7, 0x3d, 0x15, 0xc5, 0x08, 0xad, 0xfe,
 0x5b, 0x77, 0x96, 0xfb, 0xd1, 0x97, 0x4a, 0xd5, 0xd1, 0xf1, 0x17, 0x54, 0xd0, 0x9d, 0x8a, 0x1a,
 0x36, 0xec, 0xd3, 0xf4, 0x9b, 0x9a, 0xad, 0xf1, 0xbb, 0xb8, 0x45, 0x9c, 0x63, 0x34, 0xf0, 0x81,
 0x04, 0x09, 0xe6, 0x71, 0xc4, 0x78, 0xc4, 0x20, 0x9d, 0x84, 0x7d, 0xb9, 0x96, 0x06, 0xe4, 0x76,
 0xd8, 0xaf, 0xc4, 0x06, 0x96, 0xea, 0xa8, 0x42, 0x90, 0x13, 0xe9, 0xe0, 0x8c, 0xf0, 0x91, 0x6f,
 0xcc, 0xd1, 0x41, 0x3e, 0xf7, 0xb9, 0xc3, 0x38, 0x83, 0x08, 0x03, 0xbc, 0x65, 0x85, 0x38, 0x82,
 0xcd, 0x4b, 0x07, 0x45, 0xc2, 0x34, 0x8a, 0x32, 0x8a, 0xf0, 0x15, 0x45, 0x79, 0x8e, 0x30, 0x83,
 0xa2, 0xdc, 0xc0, 0x26, 0x23, 0xdb, 0xd3, 0xc1, 0x4e, 0xf1, 0x07, 0x58, 0x42, 0x79, 0x88, 0xb0,
 0x80, 0xa2, 0x4c, 0x20, 0xbc, 0x47, 0x59, 0xe6, 0x24, 0xa1, 0xf9, 0x8c, 0x01, 0x09, 0x7d, 0xa9,
 0x45, 0x28, 0xa7, 0x1e, 0xa1, 0x83, 0x7e, 0xfa, 0x69, 0x42, 0x68, 0xa6, 0x12, 0xc1, 0x61, 0x1e,
 0x64, 0x4e, 0xd4, 0x64, 0xf5, 0xc6, 0x18, 0x57, 0x88, 0x22, 0x94, 0x52, 0x40, 0x3e, 0x85, 0x08,
 0x4d, 0x74, 0x31, 0x84, 0x60, 0x62, 0x19, 0xc0, 0xf4, 0x39, 0x7f, 0x05, 0x65, 0x91, 0x3f, 0x4d,
 0x8c, 0x51, 0x89, 0xa2, 0xbc, 0x44, 0x30, 0x35, 0x99, 0xa0, 0x41, 0x78, 0x83, 0xb2, 0x8a, 0x65,
 0x18, 0x45, 0x39, 0x4d, 0x23, 0x8a, 0x72, 0x17, 0x41, 0xf6, 0x64, 0x82, 0xfd, 0x42, 0x0b, 0xa3,
 0x3c, 0xa3, 0x84, 0xcb, 0xac, 0xb1, 0x4a, 0x1d, 0x2d, 0x3c, 0x61, 0x88, 0xe3, 0x84, 0xd4, 0xcb,
 0xca, 0x00, 0xd1, 0x1c, 0xfb, 0xb3, 0x24, 0xb0, 0x6d, 0xc5, 0xd8, 0x99, 0xc0, 0x87, 0xe4, 0xe6,
 0xda, 0xfc, 0x04, 0x2f, 0x38, 0xc2, 0x21, 0x6e, 0x32, 0x42, 0x88, 0x76, 0x66, 0x59, 0xa6, 0x28,
 0x69, 0x87, 0x03, 0x81, 0x8c, 0x97, 0x25, 0x15, 0xa5, 0x93, 0xc3, 0x28, 0x09, 0x84, 0x31, 0x94,
 0x05, 0x04, 0x7b, 0x2a, 0x18, 0x5c, 0x10, 0x16, 0x51, 0x7a, 0x29, 0x45, 0x79, 0x87, 0x10, 0x47,
 0x79, 0x8c, 0xe0, 0x0a, 0x82, 0xdf, 0x76, 0x44, 0x70, 0x7e, 0x5d, 0xf2, 0x28, 0x8e, 0x21, 0xba,
 0x11, 0x5a, 0xa9, 0x4d, 0x5a, 0xec, 0xaf, 0xc2, 0x1d, 0xc1, 0xbf, 0x41, 0x96, 0x54, 0x4b, 0xaf,
 0x99, 0x0a, 0x7d, 0x76, 0xeb, 0x82, 0xe0, 0xd6, 0xc3, 0x9f, 0x24, 0x6e, 0x2e, 0x39, 0xb7, 0x85,
 0x9f, 0x24, 0xb2, 0x2d, 0x9a, 0x13, 0x9c, 0xf9, 0x0d, 0x43, 0x8e, 0xed, 0xe1, 0xe0, 0x53, 0x7d,
 0xe3, 0x00, 0x00, 0x00, 0x00, 0x49, 0x45, 0x4e, 0x44, 0xae, 0x42, 0x60, 0x82,
};

const BITMAP_OPAQUE trash24_xpm[1] = {{ png, sizeof( png ), "trash24_xpm" }};

//EOF
