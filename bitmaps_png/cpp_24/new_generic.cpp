
/* Do not modify this file, it was automatically generated by the
 * PNG2cpp CMake script, using a *.png file as input.
 */

#include <bitmaps_png/bitmaps_list.h>

static const unsigned char png[] = {
 0x89, 0x50, 0x4e, 0x47, 0x0d, 0x0a, 0x1a, 0x0a, 0x00, 0x00, 0x00, 0x0d, 0x49, 0x48, 0x44, 0x52,
 0x00, 0x00, 0x00, 0x18, 0x00, 0x00, 0x00, 0x18, 0x08, 0x06, 0x00, 0x00, 0x00, 0xe0, 0x77, 0x3d,
 0xf8, 0x00, 0x00, 0x01, 0x6d, 0x49, 0x44, 0x41, 0x54, 0x48, 0xc7, 0x63, 0xd8, 0xb9, 0x73, 0xe7,
 0x1f, 0x20, 0xfe, 0x47, 0x0e, 0x9e, 0xb2, 0x66, 0xcf, 0x45, 0xa9, 0x86, 0x23, 0x7e, 0x0c, 0xf8,
 0x00, 0x54, 0xf1, 0x7f, 0x72, 0x70, 0xff, 0xaa, 0x3d, 0x57, 0xa4, 0x1a, 0x8f, 0xfc, 0x94, 0x6a,
 0x3c, 0xec, 0x4b, 0x4b, 0x0b, 0xfe, 0xe3, 0xb5, 0x84, 0x4a, 0x16, 0x80, 0x2d, 0x91, 0x6e, 0x38,
 0xec, 0x43, 0x4b, 0x0b, 0xb0, 0x5b, 0x42, 0x89, 0x05, 0x1b, 0xb6, 0xed, 0xfe, 0xd4, 0xb6, 0x7c,
 0xef, 0x05, 0x64, 0xdc, 0xbd, 0x72, 0xef, 0x59, 0xaa, 0x59, 0x80, 0x0b, 0x8f, 0x5a, 0x40, 0x7b,
 0x0b, 0x76, 0xad, 0x5e, 0xfb, 0x7f, 0x4f, 0x4e, 0xd5, 0xff, 0x7d, 0x0e, 0xe1, 0xff, 0xf7, 0xeb,
 0xb8, 0x81, 0xf1, 0x3e, 0xe7, 0xc8, 0xff, 0x7b, 0xf2, 0x6a, 0xfe, 0xef, 0x5a, 0xbb, 0x9e, 0x32,
 0x0b, 0x76, 0xb7, 0xf6, 0xff, 0xdf, 0xa7, 0xeb, 0xfe, 0x7f, 0xbf, 0x9a, 0x23, 0x76, 0xac, 0xe7,
 0xfe, 0x7f, 0x9f, 0xaa, 0x63, 0x30, 0x59, 0x16, 0x80, 0x0c, 0xdf, 0xaf, 0xee, 0x84, 0x62, 0xe0,
 0xf9, 0xa4, 0xd2, 0xff, 0xe7, 0x12, 0x4b, 0xd0, 0x2d, 0xfa, 0x07, 0xb7, 0x84, 0x58, 0x0b, 0x40,
 0xc1, 0xb2, 0x1f, 0xdd, 0xe5, 0x40, 0xcb, 0x3e, 0xbf, 0xff, 0xf0, 0xff, 0xf3, 0xdb, 0x77, 0x18,
 0x3e, 0x39, 0xa0, 0xe6, 0xf8, 0xf1, 0x90, 0x8a, 0xad, 0x28, 0xd1, 0x16, 0xec, 0xcd, 0xad, 0xc6,
 0x0c, 0x0e, 0x0d, 0xa7, 0xff, 0x5f, 0xbf, 0x7e, 0xfd, 0xff, 0xe5, 0xf3, 0x17, 0xac, 0xc1, 0x05,
 0xf4, 0x45, 0x13, 0xd1, 0x16, 0xec, 0xb3, 0x0f, 0x25, 0xd9, 0x02, 0x20, 0x3e, 0x4f, 0xb4, 0x05,
 0xfb, 0x75, 0x5c, 0x21, 0x61, 0x9e, 0x52, 0xfe, 0xff, 0xf3, 0x87, 0x8f, 0x60, 0x83, 0xb1, 0x61,
 0x50, 0x90, 0x21, 0xc5, 0xc9, 0x17, 0xe2, 0x2d, 0x30, 0xf2, 0x26, 0xc7, 0x82, 0xef, 0xc4, 0x07,
 0x11, 0x30, 0x9d, 0x93, 0x1a, 0x44, 0xc0, 0x88, 0xbe, 0x48, 0xb4, 0x05, 0x7b, 0x8a, 0x1b, 0x48,
 0x8f, 0x03, 0x55, 0x87, 0x6a, 0xe2, 0xf3, 0xc1, 0xaa, 0xb5, 0x98, 0x19, 0x8c, 0x40, 0x32, 0x3d,
 0xac, 0x6b, 0x23, 0xc8, 0x40, 0x0a, 0x00, 0x26, 0xbb, 0x28, 0x50, 0x26, 0x22, 0x22, 0xa3, 0xfd,
 0x06, 0xba, 0xde, 0x87, 0x81, 0x1c, 0x00, 0xb5, 0xe4, 0x13, 0xce, 0xa2, 0x02, 0x28, 0xb7, 0x4f,
 0xdd, 0x31, 0x8c, 0x81, 0x12, 0x70, 0x48, 0xd3, 0x56, 0x12, 0x18, 0x04, 0x95, 0xa0, 0x74, 0x0e,
 0xc4, 0x3f, 0x80, 0xf8, 0xd7, 0x7e, 0x55, 0xc7, 0xcb, 0xfb, 0xd4, 0x1c, 0xdb, 0x40, 0x72, 0xc8,
 0x6a, 0x01, 0x37, 0xe2, 0x76, 0x71, 0xf2, 0x8f, 0xd4, 0x4b, 0x00, 0x00, 0x00, 0x00, 0x49, 0x45,
 0x4e, 0x44, 0xae, 0x42, 0x60, 0x82,
};

const BITMAP_OPAQUE new_generic_xpm[1] = {{ png, sizeof( png ), "new_generic_xpm" }};

//EOF
