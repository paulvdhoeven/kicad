
/* Do not modify this file, it was automatically generated by the
 * PNG2cpp CMake script, using a *.png file as input.
 */

#include <bitmaps_png/bitmaps_list.h>

static const unsigned char png[] = {
 0x89, 0x50, 0x4e, 0x47, 0x0d, 0x0a, 0x1a, 0x0a, 0x00, 0x00, 0x00, 0x0d, 0x49, 0x48, 0x44, 0x52,
 0x00, 0x00, 0x00, 0x18, 0x00, 0x00, 0x00, 0x18, 0x08, 0x06, 0x00, 0x00, 0x00, 0xe0, 0x77, 0x3d,
 0xf8, 0x00, 0x00, 0x00, 0xbd, 0x49, 0x44, 0x41, 0x54, 0x48, 0xc7, 0x63, 0x08, 0x09, 0x09, 0x39,
 0x0c, 0xc4, 0xff, 0x69, 0x84, 0x0f, 0x33, 0xd0, 0xd8, 0x82, 0xff, 0x0c, 0xb4, 0x02, 0x03, 0x6b,
 0x81, 0x54, 0xe3, 0x91, 0xc3, 0x20, 0x4c, 0x4b, 0x0b, 0xfe, 0x83, 0xf0, 0xf0, 0xb1, 0x20, 0x3c,
 0x3c, 0x5c, 0x33, 0x34, 0x34, 0x34, 0x04, 0x9f, 0x81, 0x20, 0x79, 0x90, 0x3a, 0x92, 0x2c, 0x00,
 0x6a, 0x72, 0x02, 0xe2, 0x53, 0xa4, 0x24, 0x4b, 0x90, 0x7a, 0x90, 0x3e, 0x92, 0x7c, 0x10, 0x1c,
 0x1c, 0xac, 0x0d, 0x54, 0x1c, 0x8a, 0xcf, 0x07, 0x40, 0x35, 0x61, 0x20, 0x75, 0x23, 0x34, 0x92,
 0x87, 0x96, 0x05, 0x32, 0xcd, 0x07, 0x55, 0x69, 0x66, 0x81, 0x4c, 0xc3, 0x11, 0x2f, 0xa0, 0xc1,
 0xef, 0xa1, 0x16, 0x9c, 0xa0, 0x9e, 0x05, 0xff, 0xff, 0x33, 0x4a, 0x35, 0x1e, 0x2e, 0x07, 0x1a,
 0xfa, 0x17, 0x6a, 0xf8, 0x26, 0xa5, 0x8e, 0x33, 0xfc, 0x54, 0xb1, 0x40, 0xb4, 0x61, 0x3f, 0x8f,
 0x64, 0xd3, 0xe1, 0xd5, 0x50, 0x83, 0xff, 0x49, 0x35, 0x1e, 0xed, 0x60, 0x68, 0xf8, 0xcf, 0x44,
 0xb5, 0xd2, 0x14, 0x14, 0x14, 0x50, 0xc3, 0xdf, 0x83, 0x82, 0x88, 0xea, 0xc5, 0x35, 0xb4, 0x68,
 0x3e, 0x0f, 0x8a, 0xdc, 0xe1, 0x53, 0xe1, 0x50, 0xd5, 0x02, 0x9a, 0x57, 0xfa, 0xb4, 0x6e, 0xb6,
 0x00, 0x00, 0x22, 0xa7, 0x95, 0x68, 0xf1, 0xe6, 0x2f, 0xf5, 0x00, 0x00, 0x00, 0x00, 0x49, 0x45,
 0x4e, 0x44, 0xae, 0x42, 0x60, 0x82,
};

const BITMAP_OPAQUE edge_to_copper_clearance_xpm[1] = {{ png, sizeof( png ), "edge_to_copper_clearance_xpm" }};

//EOF
