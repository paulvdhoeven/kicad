
/* Do not modify this file, it was automatically generated by the
 * PNG2cpp CMake script, using a *.png file as input.
 */

#include <bitmaps_png/bitmaps_list.h>

static const unsigned char png[] = {
 0x89, 0x50, 0x4e, 0x47, 0x0d, 0x0a, 0x1a, 0x0a, 0x00, 0x00, 0x00, 0x0d, 0x49, 0x48, 0x44, 0x52,
 0x00, 0x00, 0x00, 0x18, 0x00, 0x00, 0x00, 0x18, 0x08, 0x06, 0x00, 0x00, 0x00, 0xe0, 0x77, 0x3d,
 0xf8, 0x00, 0x00, 0x01, 0xf0, 0x49, 0x44, 0x41, 0x54, 0x48, 0xc7, 0x63, 0x60, 0xa0, 0x07, 0x90,
 0x6a, 0x3c, 0xfc, 0x5c, 0xaa, 0xf1, 0xc8, 0x7f, 0x72, 0xb1, 0x74, 0xe3, 0x91, 0x6d, 0xda, 0x0d,
 0x57, 0xd9, 0xf0, 0x58, 0x80, 0xa9, 0x69, 0xe7, 0xce, 0x9d, 0x70, 0x8c, 0x8d, 0x8f, 0x82, 0x9b,
 0x8e, 0x7c, 0x93, 0x6c, 0x3a, 0xb2, 0x5f, 0x65, 0xd2, 0x36, 0x76, 0xda, 0x58, 0x40, 0xc8, 0x12,
 0xaa, 0x58, 0x80, 0xcf, 0x12, 0x42, 0x16, 0xe8, 0x75, 0x1c, 0x22, 0xce, 0x02, 0x5c, 0x96, 0x10,
 0xb2, 0x00, 0x1d, 0x13, 0x11, 0xf1, 0xdf, 0xa4, 0x9a, 0x0e, 0x6f, 0xa7, 0xa5, 0x05, 0x60, 0x8c,
 0xd7, 0x02, 0x6a, 0xe0, 0xc1, 0x6b, 0x81, 0x56, 0xc5, 0xf6, 0xff, 0x85, 0x81, 0x75, 0xff, 0x17,
 0x9b, 0x85, 0xff, 0xdf, 0xad, 0xe9, 0xf2, 0x7f, 0xa7, 0x96, 0xdb, 0xff, 0x45, 0x66, 0x11, 0xff,
 0x0b, 0x82, 0x1a, 0xff, 0x6b, 0x54, 0xee, 0xa4, 0xcc, 0x02, 0xaf, 0xb4, 0xb9, 0xff, 0xb7, 0x69,
 0x7b, 0xfc, 0xdf, 0xaf, 0xe6, 0x88, 0x15, 0x83, 0xe4, 0x02, 0x92, 0x67, 0x92, 0x67, 0x41, 0x20,
 0x50, 0xe3, 0x1e, 0x75, 0xe7, 0xff, 0x87, 0x4d, 0xfc, 0xfe, 0xbf, 0xda, 0x7e, 0xe0, 0xff, 0xa5,
 0xd4, 0x0a, 0xac, 0x96, 0xec, 0x53, 0x77, 0x02, 0x5b, 0x42, 0x92, 0x05, 0x9a, 0x15, 0x3b, 0xe0,
 0x2e, 0xbf, 0x94, 0x5e, 0xf5, 0x1f, 0x04, 0x5e, 0xef, 0x3e, 0x8c, 0xd7, 0x27, 0x87, 0x54, 0x6c,
 0x45, 0x89, 0x4e, 0xa6, 0x7b, 0xf2, 0xaa, 0xe1, 0x9a, 0x2f, 0x67, 0x54, 0x43, 0x2c, 0xd8, 0x7b,
 0x14, 0xa7, 0x05, 0x60, 0x9f, 0xa8, 0x3a, 0x36, 0x11, 0x6d, 0xc1, 0x3e, 0x87, 0x30, 0x9c, 0x16,
 0x1c, 0xb5, 0x0a, 0xfe, 0x7f, 0x7f, 0xe2, 0xfc, 0xff, 0x47, 0xad, 0x43, 0xd0, 0x2c, 0x71, 0x3a,
 0x47, 0x74, 0x10, 0x21, 0x47, 0x2c, 0xba, 0x05, 0x77, 0x3a, 0xa6, 0x83, 0xf9, 0xdf, 0x1e, 0x3c,
 0xfe, 0x7f, 0xcc, 0x06, 0x61, 0xc9, 0x01, 0x35, 0xc7, 0x8f, 0xc4, 0xfb, 0xc0, 0xd8, 0x07, 0xc3,
 0x82, 0x77, 0xc7, 0xce, 0xfe, 0x3f, 0xe1, 0x1c, 0xf5, 0xff, 0xb4, 0x7f, 0xca, 0xff, 0x2f, 0xb7,
 0xee, 0x83, 0xc5, 0xbe, 0x3f, 0x79, 0xfe, 0xff, 0xb8, 0x63, 0x04, 0x4c, 0xed, 0x67, 0xe2, 0x2d,
 0x70, 0x86, 0x6b, 0xfa, 0x7f, 0x29, 0x03, 0x12, 0xc9, 0xb8, 0xc0, 0xef, 0x8f, 0x9f, 0x21, 0x3e,
 0x50, 0x77, 0x3c, 0x4b, 0x7c, 0x24, 0xe7, 0xd7, 0xc2, 0x2d, 0x00, 0x05, 0xc3, 0xa7, 0x2b, 0x37,
 0xff, 0x7f, 0x7b, 0xf4, 0x0c, 0x8e, 0x7f, 0xbc, 0x78, 0xfd, 0xff, 0xdf, 0xdf, 0xbf, 0x60, 0x0b,
 0xbe, 0xdc, 0xb8, 0x0b, 0x8b, 0x83, 0x06, 0xe2, 0x0b, 0xbb, 0x35, 0xeb, 0xff, 0xef, 0xd3, 0xf7,
 0xc4, 0x9a, 0x5a, 0x4e, 0x79, 0x25, 0x00, 0x5d, 0xfd, 0x09, 0x6c, 0xf8, 0xab, 0x9d, 0x87, 0xfe,
 0x1f, 0xd0, 0x72, 0xfd, 0xbf, 0x55, 0xc7, 0x13, 0x7f, 0x32, 0xc5, 0x86, 0xfd, 0x53, 0x66, 0xff,
 0xdf, 0x0b, 0xcc, 0x44, 0xe8, 0x16, 0xdc, 0x6a, 0x9e, 0x84, 0x62, 0xf8, 0x1e, 0x0d, 0x97, 0xff,
 0xee, 0x19, 0x0b, 0x51, 0x32, 0xda, 0x0b, 0x62, 0x2d, 0xf1, 0x4b, 0x9d, 0xfd, 0x7f, 0x8b, 0x8e,
 0x17, 0x8a, 0x05, 0x07, 0x80, 0xe5, 0xd1, 0xb9, 0x88, 0x5c, 0x30, 0x0d, 0x92, 0xf3, 0x49, 0x9b,
 0x03, 0x52, 0xfb, 0x82, 0xec, 0x56, 0xc8, 0x7e, 0x35, 0x07, 0x91, 0xfd, 0xea, 0x0e, 0xf9, 0x40,
 0x7c, 0x0c, 0x18, 0x91, 0x0f, 0x81, 0xc9, 0xf1, 0x2d, 0x50, 0xec, 0xd4, 0x3e, 0x35, 0xc7, 0x3a,
 0x90, 0x1c, 0xb2, 0x5a, 0x00, 0xb2, 0xfe, 0xd0, 0x84, 0x87, 0xf4, 0x9c, 0x30, 0x00, 0x00, 0x00,
 0x00, 0x49, 0x45, 0x4e, 0x44, 0xae, 0x42, 0x60, 0x82,
};

const BITMAP_OPAQUE save_as_xpm[1] = {{ png, sizeof( png ), "save_as_xpm" }};

//EOF
