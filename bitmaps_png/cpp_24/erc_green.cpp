
/* Do not modify this file, it was automatically generated by the
 * PNG2cpp CMake script, using a *.png file as input.
 */

#include <bitmaps_png/bitmaps_list.h>

static const unsigned char png[] = {
 0x89, 0x50, 0x4e, 0x47, 0x0d, 0x0a, 0x1a, 0x0a, 0x00, 0x00, 0x00, 0x0d, 0x49, 0x48, 0x44, 0x52,
 0x00, 0x00, 0x00, 0x18, 0x00, 0x00, 0x00, 0x18, 0x08, 0x06, 0x00, 0x00, 0x00, 0xe0, 0x77, 0x3d,
 0xf8, 0x00, 0x00, 0x00, 0x81, 0x49, 0x44, 0x41, 0x54, 0x48, 0xc7, 0x63, 0x60, 0x00, 0x02, 0xa9,
 0x96, 0x83, 0xb2, 0xd2, 0x8d, 0x47, 0xd6, 0x48, 0x35, 0x1e, 0xf9, 0x04, 0xc4, 0xff, 0x29, 0xc4,
 0x9f, 0xa4, 0x9a, 0x8e, 0xae, 0x97, 0x6a, 0x3e, 0xa2, 0xce, 0x00, 0x33, 0x1c, 0x28, 0xf8, 0x96,
 0x0a, 0x06, 0xa3, 0xe3, 0x77, 0x20, 0xb3, 0x19, 0xa0, 0x2e, 0xff, 0x4f, 0x1b, 0x7c, 0x74, 0x15,
 0x03, 0x95, 0x82, 0x05, 0x17, 0xfe, 0xc8, 0x40, 0x43, 0xc3, 0xc1, 0x78, 0xd4, 0x82, 0x51, 0x0b,
 0x46, 0x2d, 0x18, 0xb5, 0x60, 0xd4, 0x82, 0x21, 0x63, 0x01, 0x2d, 0x2b, 0x9c, 0x0f, 0x0c, 0xe0,
 0x0a, 0x9a, 0x56, 0x16, 0x34, 0x1c, 0x59, 0xc9, 0x00, 0xaa, 0xfd, 0xc1, 0x15, 0x34, 0xf5, 0x2d,
 0x78, 0x23, 0xdd, 0x70, 0x42, 0x06, 0xa9, 0x65, 0x71, 0x74, 0x15, 0xa8, 0x0e, 0xa5, 0x46, 0x3d,
 0x0c, 0x72, 0x39, 0xcc, 0x70, 0x00, 0x71, 0x2b, 0x40, 0x18, 0xfe, 0x20, 0x95, 0x85, 0x00, 0x00,
 0x00, 0x00, 0x49, 0x45, 0x4e, 0x44, 0xae, 0x42, 0x60, 0x82,
};

const BITMAP_OPAQUE erc_green_xpm[1] = {{ png, sizeof( png ), "erc_green_xpm" }};

//EOF
