
/* Do not modify this file, it was automatically generated by the
 * PNG2cpp CMake script, using a *.png file as input.
 */

#include <bitmaps_png/bitmaps_list.h>

static const unsigned char png[] = {
 0x89, 0x50, 0x4e, 0x47, 0x0d, 0x0a, 0x1a, 0x0a, 0x00, 0x00, 0x00, 0x0d, 0x49, 0x48, 0x44, 0x52,
 0x00, 0x00, 0x00, 0x18, 0x00, 0x00, 0x00, 0x18, 0x08, 0x06, 0x00, 0x00, 0x00, 0xe0, 0x77, 0x3d,
 0xf8, 0x00, 0x00, 0x02, 0x82, 0x49, 0x44, 0x41, 0x54, 0x48, 0xc7, 0xa5, 0x95, 0x4f, 0x88, 0x12,
 0x71, 0x14, 0xc7, 0xe7, 0x1c, 0x54, 0xa7, 0x2e, 0x5d, 0x82, 0x48, 0x4d, 0xaa, 0x73, 0x87, 0x28,
 0x1c, 0x35, 0x36, 0xc1, 0x5c, 0xc8, 0x3f, 0x07, 0x8f, 0xb2, 0x1e, 0xf2, 0x1f, 0xe8, 0x12, 0x49,
 0xd2, 0xc2, 0x42, 0x22, 0x4a, 0x88, 0x5b, 0x97, 0xf4, 0x96, 0x20, 0xe6, 0x5f, 0xd4, 0xf2, 0xdf,
 0x66, 0x8e, 0x2c, 0x5d, 0xa3, 0x5b, 0x87, 0xea, 0x56, 0x2c, 0x59, 0xb0, 0x97, 0x65, 0x49, 0x77,
 0x9b, 0x79, 0xfd, 0x7e, 0xe3, 0xce, 0x34, 0xea, 0x8c, 0x33, 0x6e, 0x0f, 0xbe, 0x8c, 0xbf, 0x99,
 0xf7, 0x7b, 0x9f, 0xf7, 0x7b, 0xef, 0xcd, 0x48, 0x10, 0xc8, 0x4c, 0x26, 0xd3, 0x19, 0x9b, 0xcd,
 0xf6, 0x0c, 0x69, 0x88, 0x04, 0xdd, 0x6e, 0x97, 0x15, 0xfe, 0xcd, 0x69, 0x76, 0x2d, 0xa2, 0xa1,
 0xd5, 0x6a, 0xdd, 0xb2, 0x58, 0x2c, 0xa7, 0x09, 0xa1, 0x1d, 0x07, 0x1f, 0x73, 0x8e, 0xad, 0x56,
 0x8b, 0x07, 0x34, 0x9b, 0x4d, 0x51, 0x80, 0x94, 0xcf, 0xb1, 0xc6, 0x53, 0x10, 0x4c, 0x9d, 0xcd,
 0x54, 0x28, 0xe1, 0x7d, 0xbb, 0xdd, 0x2e, 0xea, 0x33, 0xab, 0x62, 0xb1, 0x38, 0xe4, 0x01, 0x7e,
 0xbf, 0x7f, 0x4f, 0x08, 0x10, 0xcb, 0x2e, 0x95, 0x4a, 0xb1, 0xeb, 0x64, 0x32, 0xa9, 0xe4, 0x04,
 0x58, 0xfb, 0x3c, 0xa0, 0x5e, 0xaf, 0x03, 0x82, 0x2c, 0xac, 0x6f, 0x2e, 0x97, 0x63, 0x83, 0x65,
 0xb3, 0x59, 0xb9, 0x3e, 0xf0, 0xe2, 0x01, 0x3e, 0x9f, 0x0f, 0x6a, 0xb5, 0x1a, 0x04, 0x02, 0x01,
 0xc9, 0xec, 0xbc, 0x5e, 0x2f, 0xbb, 0xf6, 0x78, 0x3c, 0xa2, 0x3e, 0x4e, 0xa7, 0x73, 0x4e, 0x3c,
 0x00, 0x6f, 0xc0, 0xc1, 0xb1, 0xa3, 0x58, 0x3d, 0xc5, 0x9a, 0x3c, 0xeb, 0xd3, 0x68, 0x34, 0xe6,
 0x34, 0x05, 0x90, 0xeb, 0xc1, 0x92, 0x53, 0x34, 0x5d, 0x22, 0xa5, 0x35, 0xad, 0x54, 0x2a, 0x10,
 0x89, 0x44, 0x96, 0xef, 0x41, 0x28, 0x14, 0x82, 0x72, 0xb9, 0x0c, 0xf1, 0x78, 0x7c, 0x61, 0x76,
 0x2e, 0x97, 0x0b, 0xaa, 0xd5, 0x2a, 0x84, 0xc3, 0xe1, 0xe5, 0x4e, 0x90, 0x4e, 0xa7, 0x65, 0xe7,
 0x3a, 0x93, 0xc9, 0x80, 0xc3, 0xe1, 0x00, 0xb7, 0xdb, 0x0d, 0x78, 0xea, 0xb6, 0x5f, 0x95, 0xa0,
 0x77, 0xff, 0x21, 0xf4, 0x6f, 0xd9, 0x80, 0xba, 0x72, 0x1b, 0xa8, 0x6b, 0x2b, 0xd0, 0x37, 0x3a,
 0xa1, 0x17, 0xdc, 0x80, 0xed, 0x5a, 0x9d, 0xdd, 0xc3, 0x03, 0x82, 0xc1, 0x20, 0x94, 0x4a, 0x25,
 0x88, 0x46, 0xa3, 0xa2, 0x27, 0xc0, 0x81, 0x31, 0x00, 0x0b, 0x3f, 0x7f, 0xac, 0x37, 0x03, 0x75,
 0x15, 0x05, 0x55, 0x93, 0xa2, 0xea, 0x68, 0xf5, 0xb0, 0xbe, 0xb2, 0xba, 0x5c, 0x0f, 0x30, 0x04,
 0x03, 0x37, 0x48, 0xb3, 0x64, 0x60, 0xa1, 0xfa, 0x1a, 0x74, 0xd5, 0xe8, 0xee, 0x4c, 0x01, 0x62,
 0xb1, 0x18, 0x1b, 0x24, 0x91, 0x48, 0x88, 0x42, 0xba, 0xb9, 0x02, 0xf4, 0x2e, 0xeb, 0xd9, 0x00,
 0x5f, 0x13, 0x2f, 0x80, 0x1e, 0x1f, 0x02, 0xf3, 0x87, 0x86, 0x6f, 0x2f, 0x2b, 0x52, 0xa0, 0x5f,
 0x6f, 0x2f, 0x1a, 0xcf, 0x12, 0x72, 0xf5, 0xe7, 0x3e, 0x0f, 0xbd, 0xb5, 0xf5, 0xc9, 0x46, 0x04,
 0xa1, 0x0f, 0x8f, 0x80, 0x33, 0x86, 0x66, 0xe0, 0x83, 0xc3, 0x2b, 0x0a, 0x19, 0xa8, 0xc8, 0x07,
 0xb2, 0x00, 0x6e, 0xfe, 0xfb, 0xd7, 0x57, 0x27, 0x9b, 0xb4, 0x46, 0x90, 0x34, 0x86, 0x81, 0xdd,
 0xc2, 0x1b, 0x01, 0x44, 0xb7, 0xa3, 0x08, 0xe0, 0x40, 0x00, 0x3e, 0xab, 0x45, 0x00, 0x64, 0xf4,
 0xef, 0x91, 0xf0, 0x14, 0xbb, 0x8a, 0x4f, 0x40, 0x69, 0x0d, 0xca, 0x00, 0xa8, 0x7c, 0x02, 0xc0,
 0x0f, 0x02, 0x8d, 0x22, 0xa3, 0xa8, 0x44, 0x37, 0xee, 0x9d, 0x04, 0xf0, 0x9e, 0x28, 0x14, 0x0a,
 0xdf, 0x95, 0x00, 0x7a, 0xbe, 0x47, 0x93, 0x4d, 0x1a, 0xd4, 0xe4, 0xd1, 0x58, 0x12, 0x70, 0xb4,
 0x7f, 0xf0, 0x6f, 0x5c, 0x55, 0x64, 0x00, 0x03, 0x8c, 0x9d, 0x4e, 0x47, 0x16, 0x20, 0x1c, 0xd3,
 0x2f, 0x4f, 0x9e, 0xb3, 0xb5, 0x9e, 0xcb, 0x1e, 0x81, 0x3f, 0x6f, 0x6e, 0x71, 0x80, 0x9f, 0xaf,
 0xcf, 0xdf, 0x3d, 0xc5, 0xbe, 0x0b, 0xf9, 0x7c, 0xde, 0x80, 0xff, 0xe6, 0xda, 0xed, 0xb6, 0x34,
 0x60, 0x89, 0x17, 0x0d, 0x69, 0x34, 0xd0, 0x18, 0x48, 0xe2, 0x24, 0x86, 0x46, 0x6f, 0x6d, 0xa0,
 0x21, 0x0f, 0x16, 0x04, 0xdf, 0xa3, 0x54, 0x3a, 0x33, 0xf1, 0x3f, 0xf6, 0x4e, 0xab, 0xbf, 0x40,
 0xa9, 0xf5, 0x4f, 0x91, 0x3e, 0x0d, 0xd4, 0xe4, 0x18, 0x0b, 0x05, 0xfe, 0x88, 0xae, 0x9b, 0x3b,
 0x97, 0x6e, 0x9e, 0x13, 0xfa, 0xfe, 0x05, 0x42, 0x27, 0x48, 0x51, 0xa6, 0x12, 0xf2, 0x86, 0x00,
 0x00, 0x00, 0x00, 0x49, 0x45, 0x4e, 0x44, 0xae, 0x42, 0x60, 0x82,
};

const BITMAP_OPAQUE load_gerber_xpm[1] = {{ png, sizeof( png ), "load_gerber_xpm" }};

//EOF
