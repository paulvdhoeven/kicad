
/* Do not modify this file, it was automatically generated by the
 * PNG2cpp CMake script, using a *.png file as input.
 */

#include <bitmaps_png/bitmaps_list.h>

static const unsigned char png[] = {
 0x89, 0x50, 0x4e, 0x47, 0x0d, 0x0a, 0x1a, 0x0a, 0x00, 0x00, 0x00, 0x0d, 0x49, 0x48, 0x44, 0x52,
 0x00, 0x00, 0x00, 0x18, 0x00, 0x00, 0x00, 0x18, 0x08, 0x06, 0x00, 0x00, 0x00, 0xe0, 0x77, 0x3d,
 0xf8, 0x00, 0x00, 0x02, 0xec, 0x49, 0x44, 0x41, 0x54, 0x48, 0xc7, 0xad, 0x95, 0x59, 0x6f, 0x12,
 0x51, 0x18, 0x86, 0x51, 0x7f, 0x82, 0xc6, 0x3f, 0x60, 0xa4, 0x9b, 0xde, 0x79, 0x63, 0x52, 0x13,
 0x96, 0x16, 0x13, 0x53, 0xb4, 0x0b, 0x20, 0x60, 0x50, 0xca, 0xd2, 0xf4, 0xc6, 0xa5, 0x80, 0xb4,
 0x55, 0x59, 0x94, 0x58, 0xda, 0xb4, 0x09, 0x4a, 0x91, 0x68, 0xa2, 0x4d, 0xd3, 0x01, 0x2f, 0x80,
 0x06, 0x6b, 0xdc, 0xa0, 0x65, 0xb9, 0x6f, 0x6d, 0xff, 0x88, 0xda, 0x05, 0x7a, 0xd1, 0x0b, 0x7d,
 0x3d, 0xe7, 0x54, 0x26, 0x0c, 0x1d, 0x48, 0xa9, 0x3d, 0xc9, 0x9b, 0x7c, 0x67, 0xc9, 0xf7, 0xcc,
 0x79, 0xbf, 0x6f, 0x66, 0x24, 0x12, 0x32, 0x7c, 0x3e, 0xdf, 0xd3, 0xa1, 0xa1, 0xa1, 0xb2, 0xd7,
 0xeb, 0xc5, 0xf4, 0xf4, 0x34, 0x34, 0x1a, 0x0d, 0x53, 0x2a, 0x95, 0x82, 0xc9, 0x64, 0x62, 0xb1,
 0xdb, 0xed, 0x46, 0x38, 0x1c, 0x66, 0xf1, 0xcc, 0xcc, 0x0c, 0x32, 0x99, 0x4c, 0xb5, 0xf6, 0x88,
 0x56, 0x89, 0xd4, 0x92, 0xda, 0x41, 0x93, 0x57, 0x12, 0x7a, 0x3c, 0x9e, 0xa3, 0x02, 0x78, 0x65,
 0xb3, 0xd9, 0x3e, 0x01, 0xc0, 0x6e, 0xb7, 0x97, 0x8f, 0x13, 0x40, 0x6f, 0x22, 0x00, 0xe8, 0x74,
 0x3a, 0x1c, 0x33, 0x60, 0x0f, 0xc0, 0x09, 0x1e, 0x50, 0x49, 0x76, 0x18, 0x40, 0x24, 0x12, 0x61,
 0xb1, 0xd5, 0x6a, 0x45, 0x3a, 0x9d, 0x66, 0x09, 0xe7, 0xe6, 0xe6, 0x10, 0x08, 0x04, 0x04, 0xf2,
 0xfb, 0xfd, 0x27, 0x9b, 0x06, 0x98, 0xcd, 0x66, 0x36, 0xa7, 0x20, 0x3a, 0xa7, 0xeb, 0xc3, 0xc3,
 0xc3, 0xa0, 0x0e, 0x0c, 0x68, 0x75, 0x50, 0x1b, 0x06, 0x31, 0xa0, 0xd1, 0xb2, 0xbd, 0x44, 0x22,
 0x71, 0xaa, 0x2e, 0x20, 0x14, 0x0a, 0x09, 0x00, 0x34, 0x71, 0x65, 0x4e, 0xe3, 0x64, 0x32, 0x89,
 0xf1, 0xf1, 0x71, 0x7e, 0xad, 0x4f, 0xa7, 0x47, 0xf7, 0xc4, 0x12, 0x1e, 0x71, 0x79, 0xc8, 0xbc,
 0xef, 0xa1, 0xd1, 0x6a, 0xeb, 0x03, 0xe8, 0xd5, 0x17, 0x17, 0x17, 0xe1, 0x72, 0xb9, 0xd8, 0x7c,
 0x72, 0x72, 0x12, 0x0b, 0x0b, 0x0b, 0x30, 0x18, 0x0c, 0xfc, 0x19, 0x8b, 0xc5, 0xc2, 0xce, 0x54,
 0x3c, 0x0f, 0x25, 0x96, 0x31, 0x9b, 0x5a, 0x61, 0x31, 0x85, 0xa8, 0x8d, 0x96, 0xfa, 0x00, 0x31,
 0x48, 0x30, 0x18, 0x44, 0x3c, 0x1e, 0x87, 0xd1, 0x68, 0x44, 0xed, 0x59, 0xaa, 0xeb, 0xfa, 0xdb,
 0xe8, 0x7d, 0x91, 0x45, 0x38, 0xb9, 0x02, 0x55, 0x70, 0x89, 0xd9, 0xd5, 0x10, 0x40, 0x65, 0xb3,
 0xd9, 0x18, 0xc4, 0xe9, 0x74, 0xb2, 0xf9, 0xd4, 0xd4, 0x14, 0x38, 0x8e, 0xe3, 0x6f, 0xa2, 0xd7,
 0xeb, 0x19, 0x90, 0x4a, 0x4b, 0x2c, 0xb9, 0x41, 0x20, 0x2a, 0xf3, 0x08, 0xb3, 0xab, 0x61, 0x0d,
 0xaa, 0x45, 0x8b, 0x19, 0x8d, 0x46, 0xf9, 0x79, 0x2c, 0x16, 0x63, 0x45, 0xa5, 0xf1, 0xcb, 0xc0,
 0x73, 0xe4, 0xee, 0x3e, 0x46, 0x5e, 0xa6, 0x43, 0xbe, 0xa3, 0x0b, 0xb9, 0x76, 0x25, 0x3e, 0x5e,
 0xba, 0x86, 0xb7, 0x3d, 0x46, 0x3c, 0xb9, 0x37, 0x22, 0xde, 0x45, 0x3d, 0xc4, 0x3b, 0x99, 0x37,
 0x0e, 0xd9, 0xd8, 0x3b, 0xf4, 0xea, 0x4d, 0x70, 0x38, 0x1c, 0x02, 0xc0, 0xfc, 0xfc, 0x3c, 0x03,
 0x8c, 0x75, 0xa9, 0x91, 0xeb, 0xe8, 0x46, 0x41, 0x2a, 0x17, 0xd7, 0xc5, 0xab, 0x28, 0xb4, 0x29,
 0x6f, 0x1e, 0x00, 0x28, 0xbd, 0x31, 0x7c, 0xf9, 0x96, 0xc1, 0xd2, 0xd7, 0x2c, 0x3a, 0x1d, 0x11,
 0x51, 0xc0, 0x44, 0xff, 0x2d, 0xe4, 0x6b, 0x12, 0x6e, 0xd8, 0x46, 0xb1, 0x61, 0x79, 0x58, 0x0b,
 0xfa, 0x53, 0x6c, 0x51, 0x68, 0x05, 0x00, 0xda, 0x62, 0x9f, 0x09, 0x20, 0xf9, 0x29, 0x8b, 0xcb,
 0x0f, 0x66, 0x0f, 0x00, 0xb8, 0x57, 0xaf, 0xb1, 0xdc, 0xa6, 0x14, 0x26, 0x6a, 0x51, 0xa0, 0xb4,
 0xb5, 0x8d, 0xd2, 0xcf, 0x5f, 0x62, 0xb7, 0xd9, 0xc9, 0x9c, 0x53, 0x9d, 0xe5, 0x01, 0x6a, 0x83,
 0x19, 0x5d, 0x1e, 0x0e, 0xca, 0xd1, 0x37, 0xac, 0x58, 0xb5, 0x80, 0x0f, 0x83, 0xf7, 0x0f, 0x26,
 0x69, 0x55, 0x60, 0x77, 0x77, 0x17, 0xe5, 0x52, 0x59, 0xd4, 0xae, 0xfc, 0x79, 0xf9, 0x33, 0x41,
 0x91, 0xe9, 0x5b, 0x4c, 0x3f, 0xd9, 0x34, 0xae, 0x05, 0x64, 0x3a, 0xfb, 0x9b, 0x06, 0x14, 0xa4,
 0x8a, 0xf5, 0x43, 0x03, 0xf2, 0x17, 0x54, 0xfb, 0x9e, 0xdb, 0x47, 0x99, 0x2d, 0x34, 0xb1, 0x98,
 0xe8, 0x5e, 0x55, 0x4d, 0x76, 0x04, 0x5f, 0xd3, 0x46, 0x80, 0x5c, 0x05, 0x60, 0x75, 0xa3, 0xb4,
 0xb9, 0x55, 0x1f, 0x40, 0xf6, 0xd6, 0xef, 0x38, 0xd9, 0xd9, 0xa2, 0x54, 0xbe, 0x2d, 0xf8, 0x1f,
 0x34, 0xb4, 0xe8, 0xca, 0x40, 0xd3, 0x16, 0x15, 0x5b, 0xe4, 0xdf, 0x05, 0x7f, 0xb4, 0x46, 0x80,
 0xf4, 0x11, 0x8a, 0x4c, 0x6a, 0xe0, 0x67, 0xad, 0x4a, 0xde, 0x3c, 0xf6, 0x4f, 0x6e, 0x04, 0xe0,
 0xa2, 0xa4, 0x4d, 0xdb, 0x45, 0xda, 0x94, 0x58, 0xb2, 0xf3, 0x43, 0xb4, 0x4d, 0xb7, 0x0a, 0x52,
 0xd9, 0x19, 0x49, 0x33, 0x83, 0xb4, 0x5d, 0x3f, 0x79, 0xd1, 0x7e, 0x57, 0x27, 0x5a, 0x1f, 0x74,
 0xf1, 0x9e, 0xf3, 0xed, 0x49, 0xce, 0x14, 0xa5, 0x8a, 0x3e, 0xc9, 0x51, 0x06, 0x85, 0xec, 0x3f,
 0x9d, 0xbc, 0xde, 0xe7, 0x62, 0x93, 0x3c, 0x79, 0xaf, 0xe4, 0x7f, 0x06, 0xbd, 0x3a, 0xf5, 0x97,
 0x24, 0x5b, 0xfb, 0x07, 0xa3, 0x5a, 0x25, 0xeb, 0xbe, 0x95, 0x56, 0xe5, 0xe9, 0xea, 0xb3, 0x7f,
 0x01, 0xbb, 0x02, 0xa1, 0x48, 0x33, 0x5b, 0xd6, 0x57, 0x00, 0x00, 0x00, 0x00, 0x49, 0x45, 0x4e,
 0x44, 0xae, 0x42, 0x60, 0x82,
};

const BITMAP_OPAQUE add_board_xpm[1] = {{ png, sizeof( png ), "add_board_xpm" }};

//EOF
